<?php  // this is used to destroy all the sessions open
  // $past = time()-3600;  //that was used in an old PHP version 
  session_start();
  session_destroy();
  session_write_close();
  setcookie(session_name(),'',0,'/');
  session_regenerate_id(true);

  header('Location:/index.php');  //redirect the user to index

  ?>