<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $email = Filter::String($_POST['email']); //receive the email enter by the user and send it to filler Finction to check it
      $password = $_POST['password'];
      //make sure the user does not exist.
      
                        //function to replace the find user part in login, register and dashboard
      $User = user::findUserByEmail($email );                                                    // tranform to Function 

      //$findUser = $con->prepare("SELECT user_id, password FROM user WHERE email = LOWER(:email) LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
      //$findUser ->bindParam(':email', $email, PDO::PARAM_STR);
      //$findUser->execute();

      if ($User ){
         //user found in the database ,try to sign them in
                //transfer to Function
         //$User =  $findUser->fetch(PDO::FETCH_ASSOC); //$User is an array that contain the user data
         $user_id = (int)$User['user_id']; //the id of the user
         $hash = (string)$User['password']; //the hash (increypt) password

            if (password_verify($password, $hash)){
               //the password match, user login
               $_SESSION['user_id'] = $user_id;
               $return ['redirect'] = "/dashboard.php";
              
            }else {
            //invalid user name or password
            $return['error'] ="Please enter a valid Email/Password";
            $return ['is_logged_in'] = false;
               }
                  
      } else {
            //they need to create a new account
            $return['error'] = "You do not have an account. <a href ='/register.php'> Create one now ?</a>";
          }     	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>