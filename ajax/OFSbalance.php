<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $cell3 = $_POST['cell3'];
      $cell4 = $_POST['cell4']; //=initial balance - sum 'Quantity' from fp_trans-$cell3
      $cell2 = $_POST['cell2'];
      $TableID = $_POST['TableID'];                         
      $SumTrans = user::SumTransFP($cell2);//to get the actual Balance
     
      switch ($TableID) {
        case 'FPOutSRTbl':
          $cell4 = (integer)($SumTrans["totalBulbBal"]) -(integer)( $cell3); //balance  
          break;
        
        case 'FPInSRTbl':
          $cell4 = (integer)($SumTrans["totalBulbBal"]) +(integer)( $cell3); //balance 
          break;

        case 'FPInPrRTbl':
          $cell4 = (integer)($SumTrans["totalBulbBal"]) +(integer)( $cell3); //balance 
          break;
      }
    

      if ($SumTrans){
         //Bulb found in the database 
                         
               
               $return ['cell4'] = $cell4; //return the initial balance
              
            }else {
            //Bulb not found !!
               $return['error'] ="Bulb not found";
             }
                  
      echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>