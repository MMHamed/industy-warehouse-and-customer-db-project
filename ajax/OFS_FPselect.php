<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $bulbT = $_POST['bulbT'];
                               
      
      $SumTrans = user::SumTransFP($bulbT);

      if ($SumTrans){
         //Bulb found in the database 
                         
               $return ['cell1'] = $bulbT; //return the ID
               $return ['cell4'] = $SumTrans["totalBulbBal"]; //return the initial balance
              
            }else {
            //Bulb not found !!
               $return['error'] ="Bulb not found";
             }
                  
      echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>