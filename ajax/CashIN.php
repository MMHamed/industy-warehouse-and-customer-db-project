<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $Date = $_POST['Date']; 
      $CustID = $_POST['CustID'];
      $Amount = filter::Int($_POST['Amount']);
      $Comment = filter::String($_POST['Comment']);

               
         
         // to add new cach amount DB
         $addUser = $con->prepare("INSERT INTO customer_cash_in(  Customer_ID , dateRequest , Amount , Comment ) VALUES (:CustID, :Date, :Amount, :Comment)");
         $addUser->bindParam(':CustID', $CustID, PDO::PARAM_INT);
         $addUser->bindParam(':Date', $Date, PDO::PARAM_STR);
         $addUser->bindParam(':Amount', $Amount, PDO::PARAM_INT);
         $addUser->bindParam(':Comment', $Comment, PDO::PARAM_STR);
         $addUser->execute();

         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $Insert_id = $con ->lastInsertID();  //to get the ID created for that user
         
         $return ['message'] = 'The DataBase was updated #'. $Insert_id;  // one item of the array return named by "redirect" = the new direction file and site.

          	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>