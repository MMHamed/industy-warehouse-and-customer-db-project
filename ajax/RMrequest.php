<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      
      $index =$_POST['index'];
      $date = $_POST['Date'];
      $trans= $_POST['tran'];
      $tabName = "rm_trs";
      $idName ="Form_number";
      $Form_number = user::findLastRecord($tabName, $idName);
       if ($Form_number["Form_number"] == null ){
        $FMmber = 1;
       }else{
       $FMmber = Filter::Int($Form_number["Form_number"]) + 1; 
        };
        // to add new form request in the DB
         for ($i=0 ; $i<$index ; $i++) {
           $trans[$i][1] = Filter::Int($trans[$i][1]);
           $trans[$i][2] = Filter::Int($trans[$i][2]);
           $trans[$i][0] = Filter::Int($trans[$i][0]);
         $addReq = $con->prepare("INSERT INTO rm_trs(Form_number ,dateRequest,RawMaterial_ID,QuantityOUT,QuantityIN) 
            VALUES (:Form_number,:dateRequest,:RM_ID,:quaOUT,:quaIN) ");
         $addReq->bindParam(':Form_number', $FMmber, PDO::PARAM_INT);
         $addReq->bindParam(':dateRequest', $date, PDO::PARAM_STR);
         $addReq->bindParam(':RM_ID', $trans[$i][0], PDO::PARAM_STR);
         $addReq->bindParam(':quaOUT', $trans[$i][1], PDO::PARAM_INT);
         $addReq->bindParam(':quaIN', $trans[$i][2], PDO::PARAM_INT);
         $addReq->execute();
         };
         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $return ['added'] = 'REQUEST ADDED Form # '. $FMmber;
         $return['formNum']= $FMmber;
           // one item of the array return named by "redirect" = the new direction file and site.

          	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>