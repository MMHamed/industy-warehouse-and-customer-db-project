<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $email = Filter::String($_POST['email']); //receive the email enter by the user and send it to filler Finction to check it
         $User = user::findUserByEmail($email ); 

         //transfer to function
      //make sure the user does not exist.
      //$findUser = $con->prepare("SELECT user_id FROM user WHERE email = LOWER(:email) LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
      //$findUser ->bindParam(':email', $email, PDO::PARAM_STR);
      //$findUser->execute();

      if ($User) {
         //user found in the database
         $return['error'] ="You already have an account";
         $return ['is_logged_in'] = false;
      } else {
         //user does not exist; then we add him to the DB
         
      // to get the password from POST and then encrypt (hash) it before store it in database.
         $password = password_hash($_POST['password'], PASSWORD_DEFAULT); 
         
         // to add user in the DB
         $addUser = $con->prepare("INSERT INTO user(email,password) VALUES (LOWER(:email),:password)");
         $addUser->bindParam(':password', $password, PDO::PARAM_STR);
         $addUser->bindParam(':email', $email, PDO::PARAM_STR);
         $addUser->execute();

         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $user_id = $con ->lastInsertID();  //to get the ID created for that user
         $_SESSION['user_id'] = (int) $user_id; // (int) to make the id in the user_id number (integer)
         $return ['redirect'] = '/dashboard.php? message=WELCOME';  // one item of the array return named by "redirect" = the new direction file and site.

      }     	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>