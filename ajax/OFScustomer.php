<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $CustDet = null; //the system refuse to accept that variable without definition !!
      $custName = $_POST ['custName'];
      
      $Cust_det = user::findCustByName($custName);
       
         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         
      $CustDet  =  "العنوان : ".  $Cust_det["Address"]. "  / " . "تليفون : " . $Cust_det["PhoneNumber"] . " / " ." المسؤل : " . $Cust_det["ContactPerson"]; 
         
        $return ['detail'] = $CustDet;
                 	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>