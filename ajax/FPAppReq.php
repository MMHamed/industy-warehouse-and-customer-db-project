<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $custName = $_POST['custName'];
      $trans= $_POST['tran'];
      $index =Filter::Int($_POST['index']);
      $Adate = $_POST['Date'];
      $AformNum=$_POST['ReqNum'];
      $tabName = $_POST['TableName'];
      $ButtonID = $_POST['ButtonID'];
      switch ($ButtonID) {
        case "FPApprove":
            $i = 0;
              // to update/approve one form request in the DB
               $result1 = user::FindFormReq($AformNum,$tabName);
               
                while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                 if ($i < ($index)){               
                   try {
                  $trans[$i][0] = Filter::Int($trans[$i][0]);
                  $trans[$i][1] = Filter::Int($trans[$i][1]);  
                 $con = DB::getConnection(); 
                 $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
                 $UpdateUser = $con->prepare("UPDATE $tabName SET 
                  Cust_name =  :custName,
                  Date_app  =  :Adate,
                  Item_desc =  :descr,
                  Quantity  =  :Quantity 
                     WHERE FP_trans_ID = :OFSID ");
                 $UpdateUser->bindParam(':custName', $custName, PDO::PARAM_STR);
                 $UpdateUser->bindParam(':Adate', $Adate, PDO::PARAM_STR);
                 $UpdateUser->bindParam(':descr', $trans[$i][0], PDO::PARAM_STR);
                 $UpdateUser->bindParam(':Quantity', $trans[$i][1], PDO::PARAM_INT);
                 $UpdateUser->bindParam(':OFSID', $row1[0], PDO::PARAM_INT);
                 $UpdateUser->execute();
                 $i++;

                   }
                  catch(PDOException $e){
                  $return ['error']= $e->getMessage();
                   }
                }else {
                  $UpdateUser = $con->prepare("DELETE FROM $tabName WHERE FP_trans_ID = :transID");
                  $UpdateUser->bindParam(':transID', $row1[0], PDO::PARAM_INT);
                  $UpdateUser->execute();
               }
               };
              
               
               
               //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
               $return ['approve'] = "REQUEST was Update/Approve Request # " . $AformNum;
               
                 // one item of the array return named by "redirect" = the new direction file and site.

            break;

        case "FPReject":

              
              // to delete each one form the rejected request in the DB
               $result1 = user::FindFormReq($AformNum,$tabName);
               
                while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                  try {
                  $con = DB::getConnection(); 
                  $UpdateUser = $con->prepare("DELETE FROM $tabName WHERE FP_trans_ID = :transID");
                  $UpdateUser->bindParam(':transID', $row1[0], PDO::PARAM_INT);
                  $UpdateUser->execute();

                  }
                  catch(PDOException $e){
                  $return ['error']= $e->getMessage();
                   }
               }
               $return ['approve'] = "Request #" .$AformNum. "was Deleted";
               
              
               
               
               //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
               
               
                 // one item of the array return named by "redirect" = the new direction file and site.

           break;
      }       	

           echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)

       
         } else {
         	//die kill the script. redirect the user . do something regardless.
         	exit('Invalid URL');
         }
    

 
 ?>