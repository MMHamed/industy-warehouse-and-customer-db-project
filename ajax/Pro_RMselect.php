<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $RawM = $_POST['RawM'];
                               
      $RMrow = user::findRM($RawM); 
      //$SumTrans = user::SumTrans($RawM);
      $SumTrans = user::SumTransRM($RawM);
      if ($RMrow){
         //Bulb found in the database 
                         
               $return ['cell1'] = $RMrow['RM_ID'];  //return the ID
               $return ['cell6'] = $RMrow['Initial_Quantity'] + $SumTrans['SumQuIN']-$SumTrans['SumQuOUT'] ;  //- $SumTrans["totalBulbT"]; //return the initial balance
              
            }else {
            //Bulb not found !!
               $return['error'] ="Row Material not found";
             }
                  
      echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>