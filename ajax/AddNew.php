<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $Date = $_POST['Date']; 
      $formID = $_POST['formID'];

      switch($formID){
        case "Customer":
          $CustName=$_POST['CustName'];
          $CustAdderss = $_POST['CustAdderss'];
          $CustTel = $_POST['CustTel'];
          $CustContact = $_POST['CustContact'];
          $InitialBalance = $_POST['InitialBalance'];

          // to add new item in DB
           $addNew = $con->prepare("INSERT INTO cust_list( CustomerName , Address , PhoneNumber , ContactPerson , InitBalance ) VALUES (:CustName, :CustAdderss, :CustTel, :CustContact, :InitialBalance)");
           $addNew->bindParam(':CustName', $CustName, PDO::PARAM_STR);
           $addNew->bindParam(':CustAdderss', $CustAdderss, PDO::PARAM_STR);
           $addNew->bindParam(':CustTel', $CustTel, PDO::PARAM_INT);
           $addNew->bindParam(':CustContact', $CustContact, PDO::PARAM_STR);
           $addNew->bindParam(':InitialBalance', $InitialBalance, PDO::PARAM_INT);
           $addNew->execute();
        break;

        case "FinishProduct":
          $FPName = $_POST['FPName'];
          $InitialBalance = $_POST['InitialBalance'];

          // to add new item in DB
           $addNew = $con->prepare("INSERT INTO fp_list (BulbType , InitialQuantity ) VALUES (:FPName, :InitialBalance)");
           $addNew->bindParam(':FPName', $FPName, PDO::PARAM_STR);
           $addNew->bindParam(':InitialBalance', $InitialBalance, PDO::PARAM_INT);
           
           $addNew->execute();
        break;
        case "RawMaterial":
          $RMGroup = $_POST['RMGroup'];
          $RMName = $_POST['RMName'];
          $InitialBalance = $_POST['InitialBalance'];

          // to add new item in DB
           $addNew = $con->prepare("INSERT INTO rm_list(  Groups ,    RAW_Material , Initial_Quantity) VALUES (:RMGroup, :RMName, :InitialBalance)");
           $addNew->bindParam(':RMGroup', $RMGroup, PDO::PARAM_STR);
           $addNew->bindParam(':RMName', $RMName, PDO::PARAM_STR);
           $addNew->bindParam(':InitialBalance', $InitialBalance, PDO::PARAM_INT);
           $addNew->execute();
        break;
      };

               
         
         

         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $Insert_id = $con ->lastInsertID();  //to get the ID created for that user
         
         $return ['message'] = 'The DataBase was updated #'. $Insert_id;  // one item of the array return named by "redirect" = the new direction file and site.

          	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>