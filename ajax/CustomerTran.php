<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[]; 
      
       // define a new array (return)
      $custName = $_POST['custName'];
      $tableName = 'fp_ofs'; 
                               
      $findCustBalanceOUT = user::CustBalaceByNameList($custName,$tableName); 
      $i=0;
      while ($row = $findCustBalanceOUT->fetch(PDO::FETCH_ASSOC)){
      $return[($i)][0] = "Out to Customer"  ;
      $return[($i)][1] = $row['Form_number'] ;
      $return[($i)][2] = $row['Date_receipt'] ;
      $return[($i)][3] = $row['BulbType'] ;
      $return[($i)][4] = $row['Quantity'] ;
      $return[($i)][5] = $row['Price'] ;
      $i++;
        };

        $tableName = 'fp_ifs';
      $FindCustTranList = user::CustBalaceByNameList($custName,$tableName); 
      
      while ($row = $FindCustTranList->fetch(PDO::FETCH_ASSOC)){
      $return[($i)][0] = "In From Customer";
      $return[($i)][1] = $row['Form_number'] ;
      $return[($i)][2] = $row['Date_receipt'] ;
      $return[($i)][3] = $row['BulbType'] ;
      $return[($i)][4] = $row['Quantity'] ;
      $return[($i)][5] = $row['Price'] ;
      $i++;
        };

        $tableName = 'customer_cash_in';
        $FindCustTranList = user::CustCashIN($custName,$tableName); 
      
      while ($row = $FindCustTranList->fetch(PDO::FETCH_ASSOC)){
      $return[($i)][0] = "Cash In From Customer";
      $return[($i)][1] = $row['Cust_Cash_ID'] ;
      $return[($i)][2] = $row['dateRequest'] ;
      $return[($i)][3] = "XX" ;
      $return[($i)][4] = "XX" ;
      $return[($i)][5] = $row['Amount'] ;
      $i++;
        };
      
      $findCustBalance = user::CustBalaceByNameTotal($custName);
        $return['Balance'] =  $findCustBalance['TotCustBal'];


      if (!isset($return)){
      $return['error'] ="Raw Material not found";
      };
                  
      echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>