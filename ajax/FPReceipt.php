<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $custName = $_POST['custName'];
      $trans= $_POST['tran'];
      $index =Filter::Int($_POST['index']);
      $Rdate = $_POST['Date'];
      $RformNum=$_POST['ReqNum'];
      $tabName = $_POST['TableName'];
      
            $i = 0;
              // to update/approve one form request in the DB
               $result1 = user::FindFormReq($RformNum,$tabName);
               
                while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                                
                   try {
                  $con = DB::getConnection(); 
                 $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
                 $UpdateUser = $con->prepare("UPDATE $tabName SET 
                  Date_receipt  =  :Rdate,
                  Price  =  :Price 
                     WHERE FP_trans_ID = :OFSID ");
                 $UpdateUser->bindParam(':Rdate', $Rdate, PDO::PARAM_STR);
                 $UpdateUser->bindParam(':Price', $trans[$i], PDO::PARAM_INT);
                 $UpdateUser->bindParam(':OFSID', $row1[0], PDO::PARAM_INT);
                 $UpdateUser->execute();
                 $i++;

                   }
                  catch(PDOException $e){
                  $return ['error']= $e->getMessage();
                   }
                
               };
              
               
               
               //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
               $return ['approve'] = "DataBase was Update Request # " . $RformNum;
               
                 // one item of the array return named by "redirect" = the new direction file and site.

            

           echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)

       
         } else {
         	//die kill the script. redirect the user . do something regardless.
         	exit('Invalid URL');
         }
    

 
 ?>