<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $trans= $_POST['tran'];
      $index =Filter::Int($_POST['index']);
      $Adate = Filter::String($_POST['Date']);
      $AformNum=Filter::Int($_POST['ReqNum']);
      $tabName = "rm_trs";
      $i = 0;
        // to update/approve one form request in the DB
         $result1 = user::FindFormReq($AformNum,$tabName);
         
          while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
           if ($i < ($index)){               
             try {
             $trans[$i][1] = filter::Int($trans[$i][1]);
             $trans[$i][0] = filter::Int($trans[$i][0]); 
           $con = DB::getConnection(); 
           $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
           $UpdateUser = $con->prepare("UPDATE $tabName SET 
            DateApprove    =  :Adate,
            QuantityIN    =  :QuantityIN,
            QuantityOUT   =  :QuantityOUT
               WHERE RM_TRS_ID=:RMID ");
           $UpdateUser->bindParam(':Adate', $Adate, PDO::PARAM_STR);
           $UpdateUser->bindParam(':QuantityIN', $trans[$i][1], PDO::PARAM_INT);
           $UpdateUser->bindParam(':QuantityOUT', $trans[$i][0], PDO::PARAM_INT);
           $UpdateUser->bindParam(':RMID', $row1[0], PDO::PARAM_INT);
           $UpdateUser->execute();
           $i++;

             }
            catch(PDOException $e){
            $return ['error']= $e->getMessage();
             }
          }else {
            $UpdateUser = $con->prepare("DELETE FROM $tabName WHERE RM_TRS_ID = :RMID");
            $UpdateUser->bindParam(':RMID', $row1[0], PDO::PARAM_INT);
            $UpdateUser->execute();
         }
         };
        
         
         
         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $return ['approve'] = "REQUEST was Update/Approve Request # " . $AformNum;
         
           // one item of the array return named by "redirect" = the new direction file and site.

          	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>