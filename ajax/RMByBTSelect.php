<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[]; 
      
       // define a new array (return)
      $SelBT = $_POST['SelBT'];
                               
      $findBSC = user::findBSCFromBT($SelBT); 
      
      while ($row = $findBSC->fetch(PDO::FETCH_ASSOC)){

      $return[$row['RM_B_ID']] = $row['Body_SKD_CKD'];
        };
       
      if (!isset($return)){
      $return['error'] ="Raw Material not found";
      };
                  
      echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>