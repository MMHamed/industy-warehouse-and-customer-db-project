<?php
 // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php'; // "../" mean that you go up one step and then search for inc


   if ($_SERVER['REQUEST_METHOD'] == 'POST') { //if there is a POST sent from main.js
   	 //always return jason format
   	header('Content-Type: application/json');
     	$return=[];  // define a new array (return)
      $custName = $_POST ['custName'];
      $trans= $_POST['tran'];
      $index =$_POST['index'];
      $date = $_POST['Date'];
      $SubID = $_POST['SubID'];
      switch ($SubID) {
        case 'OFSsubmit':
          $tabName = "fp_ofs";
          break;
       case 'IFSsubmit':
          $tabName = "fp_ifs";
          break;
        case 'IFPsubmit':
          $tabName = "fp_ifp";
          break; 
       
      }

      $idName ="Form_number";
      $Form_number = user::findLastRecord($tabName, $idName);
       if ($Form_number["Form_number"] == null){
        $Form_number["Form_number"] = 1;
       }else{
       $Form_number["Form_number"] = Filter::Int($Form_number["Form_number"])+1; 
        };
        // to add new form request in the DB
         for ($i=0 ; $i<$index ; $i++) {
           
         $trans[$i][1] = Filter::Int($trans[$i][1]);
         $trans[$i][0] = Filter::Int($trans[$i][0]);
         $addUser = $con->prepare("INSERT INTO $tabName (Form_number ,Cust_name,Date_req,Item_desc,Quantity) 
            VALUES (:Form_number,:custName,:dat,:descr,:qua )  ");
         $addUser->bindParam(':Form_number', $Form_number["Form_number"], PDO::PARAM_STR);
         $addUser->bindParam(':custName', $custName, PDO::PARAM_STR);
         $addUser->bindParam(':dat', $date, PDO::PARAM_STR);
         $addUser->bindParam(':descr', $trans[$i][0], PDO::PARAM_INT);
         $addUser->bindParam(':qua', $trans[$i][1], PDO::PARAM_INT);
         $addUser->execute();
         };
         //after adding the user to the database he will open a session with the user id and redirect him to the dashboard
         $return ['added'] = 'REQUEST ADDED Form # '. $Form_number["Form_number"];
         $return['formNum']= $Form_number["Form_number"];
           // one item of the array return named by "redirect" = the new direction file and site.

          	

     echo json_encode($return , JSON_PRETTY_PRINT); exit; //send that back to JS by using JSON format (main.js)
   } else {
   	//die kill the script. redirect the user . do something regardless.
   	exit('Invalid URL');
   }
 ?>