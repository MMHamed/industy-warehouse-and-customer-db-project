-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 04, 2019 at 04:49 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_course`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_cash_in`
--

DROP TABLE IF EXISTS `customer_cash_in`;
CREATE TABLE IF NOT EXISTS `customer_cash_in` (
  `Cust_Cash_ID` int(20) NOT NULL AUTO_INCREMENT COMMENT 'the Request ID',
  `Customer_ID` int(20) NOT NULL COMMENT 'the IN of the customer Name',
  `dateRequest` date NOT NULL COMMENT 'Date of the money received',
  `Amount` int(200) NOT NULL COMMENT 'amount of the money ',
  `Comment` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Any Extra Comment',
  PRIMARY KEY (`Cust_Cash_ID`),
  KEY `Customer_ID` (`Customer_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_cash_in`
--

INSERT INTO `customer_cash_in` (`Cust_Cash_ID`, `Customer_ID`, `dateRequest`, `Amount`, `Comment`) VALUES
(1, 1, '2019-06-03', 100, 'cach'),
(2, 1, '2019-06-03', 200, 'check'),
(3, 2, '2019-06-03', 300, ''),
(4, 3, '2019-06-03', 400, ''),
(5, 4, '2019-06-03', 100, 'check'),
(6, 4, '2019-06-03', 100, ''),
(7, 4, '2019-06-03', 100, ''),
(8, 8, '2019-06-03', 10, '');

-- --------------------------------------------------------

--
-- Table structure for table `cust_list`
--

DROP TABLE IF EXISTS `cust_list`;
CREATE TABLE IF NOT EXISTS `cust_list` (
  `Cust_ID` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Customer ID',
  `CustomerName` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(65) CHARACTER SET utf8 DEFAULT NULL,
  `PhoneNumber` int(11) DEFAULT NULL,
  `ContactPerson` varchar(7) CHARACTER SET utf8 DEFAULT NULL,
  `InitBalance` int(20) DEFAULT NULL,
  PRIMARY KEY (`Cust_ID`),
  KEY `CustomerName` (`CustomerName`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cust_list`
--

INSERT INTO `cust_list` (`Cust_ID`, `CustomerName`, `Address`, `PhoneNumber`, `ContactPerson`, `InitBalance`) VALUES
(1, 'الشركه الهندسيه', 'الشيخ زايد بفرلى هيلز امام البوابه الرباعيه', 1222222222, 'احمد 1', NULL),
(2, 'الشركه الهندسيه', '15 ش سليمان الحلبى التوفيقيه', 1222222223, 'احمد 2', NULL),
(3, 'بفرلى هيلز', 'الكيلو 38 طريق القاهره اسكندريه الصحراوى الشيخ زايد الجيزه', 1222222224, 'احمد 3', NULL),
(4, 'موسسه التعمير للتجاره والمقاولات', 'المهندسين 55 ميدان موسى جلال متفرع من فوزى رماح - متفرع من ش شهاب', 1222222225, 'احمد 4', NULL),
(5, 'البير كرم', '201ش شبرا امام العزبى', 1222222226, 'احمد 5', NULL),
(6, 'الموسسه المصريه', '145 برج اليسر - المحموديه', 1222222227, 'احمد 6', NULL),
(7, 'باور الكتروينك', 'المقطم 86ش 9 امام معهد الطيران', 1222222228, 'احمد 7', NULL),
(8, 'بيت القوى الكهربيه', '14 ش الحمهوريه عابدين', 1222222229, 'احمد 8', NULL),
(9, 'شركه الايمان', '20 ش عماد الدين وسط البلد', 1222222230, 'احمد 9', NULL),
(10, 'الفادى', '151ش شبرا', 1222222231, 'احمد 10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fp_ifp`
--

DROP TABLE IF EXISTS `fp_ifp`;
CREATE TABLE IF NOT EXISTS `fp_ifp` (
  `FP_trans_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'INDEX',
  `Form_number` int(10) NOT NULL COMMENT 'Request Form Number',
  `Cust_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Customer Name ID',
  `Date_req` date DEFAULT NULL COMMENT 'date Of The Request',
  `Date_app` date DEFAULT NULL COMMENT 'Date Of The approve',
  `Date_receipt` date DEFAULT NULL COMMENT 'Date ',
  `Item_desc` int(55) NOT NULL COMMENT 'Item Decription ID',
  `Quantity` int(10) NOT NULL COMMENT 'Quantity of the requested Item',
  PRIMARY KEY (`FP_trans_ID`),
  KEY `Item_desc` (`Item_desc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fp_ifp`
--

INSERT INTO `fp_ifp` (`FP_trans_ID`, `Form_number`, `Cust_name`, `Date_req`, `Date_app`, `Date_receipt`, `Item_desc`, `Quantity`) VALUES
(3, 1, 'Production', '2019-05-24', NULL, NULL, 1, 10),
(4, 1, 'Production', '2019-05-24', NULL, NULL, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `fp_ifs`
--

DROP TABLE IF EXISTS `fp_ifs`;
CREATE TABLE IF NOT EXISTS `fp_ifs` (
  `FP_trans_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Form_number` int(10) NOT NULL,
  `Cust_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Customer Name ID',
  `Date_req` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date_app` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date_receipt` date DEFAULT NULL COMMENT 'Date of the receipt IN',
  `Item_desc` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Item Description ID',
  `Quantity` int(10) NOT NULL,
  `Price` int(20) DEFAULT NULL COMMENT 'The price of the FP in receipt IN',
  PRIMARY KEY (`FP_trans_ID`),
  KEY `Cust_name` (`Cust_name`),
  KEY `Item_desc` (`Item_desc`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fp_ifs`
--

INSERT INTO `fp_ifs` (`FP_trans_ID`, `Form_number`, `Cust_name`, `Date_req`, `Date_app`, `Date_receipt`, `Item_desc`, `Quantity`, `Price`) VALUES
(1, 1, '1', '2019-05-24', '2019-05-24', '2019-05-29', '1', 10, 110),
(2, 1, '1', '2019-05-24', '2019-05-24', '2019-05-29', '2', 10, 220),
(3, 2, '2', '2019-05-24', '2019-05-24', '2019-05-29', '3', 10, 110),
(4, 2, '2', '2019-05-24', '2019-05-24', '2019-05-29', '4', 10, 220),
(5, 3, '3', '2019-05-24', NULL, NULL, '9', 10, NULL),
(6, 3, '3', '2019-05-24', NULL, NULL, '7', 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fp_list`
--

DROP TABLE IF EXISTS `fp_list`;
CREATE TABLE IF NOT EXISTS `fp_list` (
  `FP_ID` int(5) NOT NULL AUTO_INCREMENT,
  `BulbType` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `InitialQuantity` int(10) DEFAULT NULL,
  UNIQUE KEY `FP_ID` (`FP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fp_list`
--

INSERT INTO `fp_list` (`FP_ID`, `BulbType`, `InitialQuantity`) VALUES
(1, 'Bulb (3W) 6000K P', 100),
(2, 'Bulb (3W) 3000K P', 200),
(3, 'Bulb (5W) 6000K P', 300),
(4, 'Bulb (5W) 3000K P', 400),
(5, 'Bulb (7W) 6000K P', 500),
(6, 'Bulb (7W) 3000K P', 600),
(7, 'Candel (5W) 6000K', 700),
(8, 'Candel (5W) 3000K', 800),
(9, 'Candel Tip (5W) 6000K', 900),
(10, 'Candel Tip (5W) 3000K', 1000),
(11, 'MR 16 (5W) 6000K', 1100),
(12, 'MR 16 (5W) 3000K', 1200),
(13, 'Bulb (9W) 6000K P', 1300),
(14, 'Bulb (9W) 3000K P', 1400),
(15, 'Bulb (9W) 6000K C', 1500),
(16, 'Bulb (9W) 3000K C', 1600),
(17, 'Bulb (12W) 6000K P', 1700),
(18, 'Bulb (12W) 3000K P', 1800),
(19, 'Bulb (12W) 6000K C', 1900),
(20, 'Bulb (12W) 3000K C', 2000),
(21, 'Bulb (15W) 6000K P', 2100),
(22, 'Bulb (15W) 3000K P', 2200),
(23, 'Bulb (15W) 6000K C', 2300),
(24, 'Bulb (15W) 3000K C', 2400),
(25, 'Bulb (20W) 6000K', 2500),
(26, 'Bulb (20W) 3000K', 2600),
(27, 'Bulb (40W) 6000K', 2700),
(28, 'Bulb (40W) 3000K', 2800),
(29, 'DOWNLIGHT (3W) 6000K', 2900),
(30, 'DOWNLIGHT (3W) 3000K', 3000),
(31, 'DOWNLIGHT (6W) 6000K', 3100),
(32, 'DOWNLIGHT (6W) 3000K', 3200),
(33, 'DOWNLIGHT (18W) 6000K', 3300),
(34, 'DOWNLIGHT (18W) 3000K', 3400),
(35, 'Tube 60 cm (8W) 6000K', 3500),
(36, 'Tube 60 cm (8W) 3000K', 3600),
(37, 'Tube 120 cm (18W) 6000K', 3700),
(38, 'Tube 120 cm (18W) 3000K', 3800),
(39, 'Panel 60*60 (40W) 6000K', 3900),
(40, 'Panel 60*60 (40W) 3000K', 4000),
(41, 'Panel 60*60 (48W) 6000K', 4100),
(42, 'Panel 60*60 (48W) 3000K', 4200),
(43, 'Filament A70 (10W) 6000K', 4300),
(44, 'Filament A70 (10W) 3000K', 4400),
(45, 'Filament A60 (8W) 6000K', 4500),
(46, 'Filament A60 (8W) 3000K', 4600),
(47, 'Filament G45 (4W) 6000K', 4700),
(48, 'Filament G45 (4W) 3000K', 4800),
(49, 'Filament C35 (4W) 6000K', 4900),
(50, 'Filament C35 (4W) 3000K', 5000),
(51, 'Filament T35 (4W) 6000K', 5100),
(52, 'Filament T35 (4W) 3000K', 5200);

-- --------------------------------------------------------

--
-- Table structure for table `fp_ofs`
--

DROP TABLE IF EXISTS `fp_ofs`;
CREATE TABLE IF NOT EXISTS `fp_ofs` (
  `FP_trans_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'index',
  `Form_number` int(10) NOT NULL,
  `Cust_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'customer name',
  `Date_req` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'date of request',
  `Date_app` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'date of the approval',
  `Date_receipt` date DEFAULT NULL COMMENT 'date of printing the Receipt',
  `Item_desc` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Finish Product item description',
  `Quantity` int(10) NOT NULL COMMENT 'Quantity requested/approve',
  `Price` int(20) DEFAULT NULL COMMENT 'The price of the FP in receipt',
  PRIMARY KEY (`FP_trans_ID`),
  KEY `Cust_name` (`Cust_name`),
  KEY `Item_desc` (`Item_desc`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fp_ofs`
--

INSERT INTO `fp_ofs` (`FP_trans_ID`, `Form_number`, `Cust_name`, `Date_req`, `Date_app`, `Date_receipt`, `Item_desc`, `Quantity`, `Price`) VALUES
(1, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '1', 5, 55),
(2, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '2', 5, 110),
(3, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '3', 5, 165),
(4, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '4', 5, 220),
(5, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '5', 5, 275),
(6, 2, '3', '2019-04-24', '2019-05-22', '2019-05-29', '1', 9, 198),
(7, 2, '3', '2019-04-24', '2019-05-22', '2019-05-29', '2', 9, 297),
(8, 2, '3', '2019-04-24', '2019-05-22', '2019-05-29', '3', 9, 396),
(11, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '1', 4, 44),
(12, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '2', 4, 88),
(13, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '3', 4, 1332),
(14, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '4', 4, 176),
(15, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '5', 4, 220),
(16, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '6', 4, 264),
(17, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '7', 4, 308),
(18, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '8', 4, 352),
(19, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '9', 4, 396),
(20, 3, '10', '2019-04-24', '2019-05-20', '2019-05-29', '10', 9, 900),
(29, 5, '7', '2019-05-21', '2019-05-26', '2019-05-29', '1', 10, 220),
(32, 1, '2', '2019-04-24', '2019-05-20', '2019-05-29', '1', 5, 330);

-- --------------------------------------------------------

--
-- Table structure for table `rm_bsc_list`
--

DROP TABLE IF EXISTS `rm_bsc_list`;
CREATE TABLE IF NOT EXISTS `rm_bsc_list` (
  `RM_B_ID` int(3) NOT NULL,
  `BulbType` varchar(17) CHARACTER SET utf8 DEFAULT NULL,
  `Body_SKD_CKD` varchar(29) CHARACTER SET utf8 DEFAULT NULL,
  `Goups` varchar(37) CHARACTER SET utf8 DEFAULT NULL,
  `RAW_Material` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `RM_ID` int(3) DEFAULT NULL,
  PRIMARY KEY (`RM_B_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rm_bsc_list`
--

INSERT INTO `rm_bsc_list` (`RM_B_ID`, `BulbType`, `Body_SKD_CKD`, `Goups`, `RAW_Material`, `RM_ID`) VALUES
(1, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(6000K)', 2),
(2, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (3W)', 38),
(3, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cup(0403-00207) (G45 3W)', 224),
(4, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cover(0402-00107) (G45 3W)', 225),
(5, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'AL-BASE-white-E27(دوايه)', 245),
(6, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'BOX (5W+ 3W)(خارجيه)', 292),
(7, 'Bulb_(3W)_6000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'lamp box 3W / White', 297),
(8, 'Bulb_(3W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(6000K)', 2),
(9, 'Bulb_(3W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (CKD) (Store 3)', 'CKD-G45-18V2835-7A-1.1 (G45)(3/5W)', 70),
(10, 'Bulb_(3W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'LED Source/1206-00008(white)', 117),
(11, 'Bulb_(3W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/1206-0R0 (0201-00057)(C35/G45/7W/9W )', 150),
(12, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (3W)', 38),
(13, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (3W)', 37),
(14, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(15, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(16, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(17, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Inductor/(0205-00008) 222K (C35/G45/7W/ MR16)', 215),
(18, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Transformer/6.6MH (0206-00019)(G45)', 195),
(19, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Resistor (0202-00036)27Ω (C35/G45/10W/8W)', 219),
(20, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire red(24)', 340),
(21, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire White(28)', 341),
(22, 'Bulb_(3W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire Yellow(28)', 342),
(23, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (3W)', 37),
(24, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(CKD) (Store 3)', 'CKD-JTX-L37DHA-V0(C35/G45/MR16)', 99),
(25, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip diode(0201-00094)/ES1J (20W/15W/9W/40W/7W/MR16/C35/G45)', 133),
(26, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'bridge rectifier (0210-00003) MB10S (C35/ G45/7W/ MR16)', 127),
(27, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip capacitor(0203-0006)/1UF/25V (G45/ A60/ MR16/C35)', 137),
(28, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'IC/ (1102-00013)LA5821(C35/G45/7W/MR16/8W/10 W)', 146),
(29, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/ (0201-00037) 4K7 (C35 /G45/ 7W / MR16)', 174),
(30, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/200K(0210-00072) (G45)', 184),
(31, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/6R2 (0201-00053) (G45)', 165),
(32, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(33, 'Bulb_(3W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(34, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(3000K)', 1),
(35, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (3W)', 38),
(36, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cup(0403-00207) (G45 3W)', 224),
(37, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cover(0402-00107) (G45 3W)', 225),
(38, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'AL-BASE-white-E27(دوايه)', 245),
(39, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'BOX (5W+ 3W)(خارجيه)', 292),
(40, 'Bulb_(3W)_3000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'lamp box 3W /Yellow', 298),
(41, 'Bulb_(3W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(3000K)', 1),
(42, 'Bulb_(3W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (CKD) (Store 3)', 'CKD-G45-18V2835-7A-1.1 (G45)(3/5W)', 70),
(43, 'Bulb_(3W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'LED Source/1206-00018 (yellow)', 114),
(44, 'Bulb_(3W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/1206-0R0 (0201-00057)(C35/G45/7W/9W )', 150),
(45, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (3W)', 38),
(46, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (3W)', 37),
(47, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(48, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(49, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(50, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Inductor/(0205-00008) 222K (C35/G45/7W/ MR16)', 215),
(51, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Transformer/6.6MH (0206-00019)(G45)', 195),
(52, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Resistor (0202-00036)27Ω (C35/G45/10W/8W)', 219),
(53, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire red(24)', 340),
(54, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire White(28)', 341),
(55, 'Bulb_(3W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire Yellow(28)', 342),
(56, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (3W)', 37),
(57, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(CKD) (Store 3)', 'CKD-JTX-L37DHA-V0(C35/G45/MR16)', 99),
(58, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip diode(0201-00094)/ES1J (20W/15W/9W/40W/7W/MR16/C35/G45)', 133),
(59, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'bridge rectifier (0210-00003) MB10S (C35/ G45/7W/ MR16)', 127),
(60, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip capacitor(0203-0006)/1UF/25V (G45/ A60/ MR16/C35)', 137),
(61, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'IC/ (1102-00013)LA5821(C35/G45/7W/MR16/8W/10 W)', 146),
(62, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/ (0201-00037) 4K7 (C35 /G45/ 7W / MR16)', 174),
(63, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/200K(0210-00072) (G45)', 184),
(64, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/6R2 (0201-00053) (G45)', 165),
(65, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(66, 'Bulb_(3W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(67, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(6000K)', 4),
(68, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (5W)', 40),
(69, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cup(0403-00207) (G45 3W)', 224),
(70, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cover(0402-00107) (G45 3W)', 225),
(71, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'PARTS (Store 1)', 'AL-BASE-white-E27(دوايه)', 245),
(72, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'BOX (5W+ 3W)(خارجيه)', 292),
(73, 'Bulb_(5W)_6000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'lamp box 5W / White', 299),
(74, 'Bulb_(5W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(6000K)', 4),
(75, 'Bulb_(5W)_6000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (CKD) (Store 3)', 'CKD-G45-18V2835-7A-1.1 (G45)(3/5W)', 70),
(76, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (5W)', 40),
(77, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (5W)', 39),
(78, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(79, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(80, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(81, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Inductor/(0205-00008) 222K (C35/G45/7W/ MR16)', 215),
(82, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Transformer /5MH (0206-00067)(C35/ MR16/7W)', 194),
(83, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Resistor (0202-00036)27Ω (C35/G45/10W/8W)', 219),
(84, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire red(24)', 340),
(85, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire White(28)', 341),
(86, 'Bulb_(5W)_6000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire Yellow(28)', 342),
(87, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (5W)', 39),
(88, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(CKD) (Store 3)', 'CKD-JTX-L37DHA-V0(C35/G45/MR16)', 99),
(89, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip diode(0201-00094)/ES1J (20W/15W/9W/40W/7W/MR16/C35/G45)', 133),
(90, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'bridge rectifier (0210-00003) MB10S (C35/ G45/7W/ MR16)', 127),
(91, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip capacitor(0203-0006)/1UF/25V (G45/ A60/ MR16/C35)', 137),
(92, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'IC/ (1102-00013)LA5821(C35/G45/7W/MR16/8W/10 W)', 146),
(93, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/ (0201-00037) 4K7 (C35 /G45/ 7W / MR16)', 174),
(94, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/510K (0201-000103)(7W/C35)', 188),
(95, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(96, 'Bulb_(5W)_6000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(97, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(3000K)', 3),
(98, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (5W)', 40),
(99, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cup(0403-00207) (G45 3W)', 224),
(100, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'cover(0402-00107) (G45 3W)', 225),
(101, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'PARTS (Store 1)', 'AL-BASE-white-E27(دوايه)', 245),
(102, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'BOX (5W+ 3W)(خارجيه)', 292),
(103, 'Bulb_(5W)_3000K_P', 'Body_SKD', 'Manufacture_material (Roof)', 'lamp box 5W /Yellow', 300),
(104, 'Bulb_(5W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (SKD) (Store 3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(3000K)', 3),
(105, 'Bulb_(5W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'LIGHT_SOURCE (CKD) (Store 3)', 'CKD-G45-18V2835-7A-1.1 (G45)(3/5W)', 70),
(106, 'Bulb_(5W)_3000K_P', 'LIGHT_SOURCE (SKD) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'LED Source/1206-00018 (yellow)', 114),
(107, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SKD-JTX-L37DHA-V0 (5W)', 40),
(108, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (5W)', 39),
(109, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(110, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(111, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'CAPACITOR (0204-00008) 2.2MF,400V-6.3*12 (C35/ G45/7W )', 200),
(112, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Inductor/(0205-00008) 222K (C35/G45/7W/ MR16)', 215),
(113, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Transformer /5MH (0206-00067)(C35/ MR16/7W)', 194),
(114, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'ELECTRICAL_COMPONENTS (store 3)', 'Resistor (0202-00036)27Ω (C35/G45/10W/8W)', 219),
(115, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire red(24)', 340),
(116, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire White(28)', 341),
(117, 'Bulb_(5W)_3000K_P', 'DRIVER(SMT-->SKD) (Store 3)', 'Manufacture_material (Roof)', 'Selicon Wire Yellow(28)', 342),
(118, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(SMT/SKD) (Store 3)', 'SMT-JTX-L37DHA-V0 (5W)', 39),
(119, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'DRIVER(CKD) (Store 3)', 'CKD-JTX-L37DHA-V0(C35/G45/MR16)', 99),
(120, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip diode(0201-00094)/ES1J (20W/15W/9W/40W/7W/MR16/C35/G45)', 133),
(121, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'bridge rectifier (0210-00003) MB10S (C35/ G45/7W/ MR16)', 127),
(122, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip capacitor(0203-0006)/1UF/25V (G45/ A60/ MR16/C35)', 137),
(123, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'IC/ (1102-00013)LA5821(C35/G45/7W/MR16/8W/10 W)', 146),
(124, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/ (0201-00037) 4K7 (C35 /G45/ 7W / MR16)', 174),
(125, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/510K (0201-000103)(7W/C35)', 188),
(126, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168),
(127, 'Bulb_(5W)_3000K_P', 'DRIVER(CKD --> SMT) (Store 3)', 'ELECTRICAL_COMPONENTS_ROLES (Store 3)', 'chip resistor/13R0 (0201-00107)(C35)', 168);

-- --------------------------------------------------------

--
-- Table structure for table `rm_list`
--

DROP TABLE IF EXISTS `rm_list`;
CREATE TABLE IF NOT EXISTS `rm_list` (
  `RM_ID` int(20) NOT NULL AUTO_INCREMENT COMMENT 'the Index and the code',
  `Groups` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RAW_Material` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Initial_Quantity` int(5) DEFAULT NULL,
  PRIMARY KEY (`RM_ID`),
  KEY `Groups` (`Groups`,`RAW_Material`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rm_list`
--

INSERT INTO `rm_list` (`RM_ID`, `Groups`, `RAW_Material`, `Initial_Quantity`) VALUES
(1, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(3000K)', 100),
(2, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-G45-18V2835-7A-1.1 (G45)(3W)(6000K)', 200),
(3, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(3000K)', 300),
(4, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-G45-18V2835-7A-1.1 (G45)(5W)(6000K)', 400),
(5, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-C35-18V2835-7A-1.0 (C35)(3000K)', 500),
(6, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-C35-18V2835-7A-1.0 (C35)(6000K)', 600),
(7, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-LED-C37-2835-9V-8-1.0 (5W Candle) (3000k)', 700),
(8, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-LED-C37-2835-9V-8-1.0(5W Candle) (6000k)', 800),
(9, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-10E-1.5(7W)(3000K)', 900),
(10, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-10E-1.5(7W)(6000K)', 1000),
(11, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-A60-9V2835-20G-1.0 (9W)(3000K)', 1100),
(12, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-A60-9V2835-20G-1.0 (9W)(6000K)', 1200),
(13, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-V4-A60-10A-1.0(9W Ceramic)(3000K)', 1300),
(14, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-V4-A60-10A-1.0(9W Ceramic)(6000K)', 1400),
(15, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-10A-1.1(9W Plastic) (3000k)', 1500),
(16, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-10A-1.1(9W Plastic)(6000k)', 1600),
(17, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20E-V1.0 (9W A70)(3000k)', 1700),
(18, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20E-V1.0 (9W A70)(6000k)', 1800),
(19, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20B-1.0(12W Ceramic)(6000k)', 1900),
(20, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20B-1.0(12W Ceramic)(3000k)', 2000),
(21, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20A-1.2(12W Plastic)(3000k)', 2100),
(22, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A60-9V2835-20A-1.2(12W Plastic)(6000k)', 2200),
(23, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-A70-9V2835-27A-1.0 (15ًW new)(3000K)', 2300),
(24, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-A70-9V2835-27A-1.0 (15ًW new)(6000K)', 2400),
(25, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A70-9V2835-26A-1.1(15W Ceramic)(3000k)', 2500),
(26, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A70-9V2835-26A-1.1(15W Ceramic)(6000k)', 2600),
(27, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A70-9V2835-26B-1.1(15W Plastic) (3000k)', 2700),
(28, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-A70-9V2835-26B-1.1(15W Plastic)(6000k)', 2800),
(29, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-T20-9V2835-40A-1.2(0211-00110) (20W)(3000K)', 2900),
(30, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-T20-9V2835-40A-1.2(0211-00110) (20W)(6000K)', 3000),
(31, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-T40-9V2835-84A-1.1(40W)(3000K)', 3100),
(32, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-JTX-T40-9V2835-84A-1.1(40W)(6000K)', 3200),
(33, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-MR16-7C(MR16 new)(3000K)', 3300),
(34, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-MR16-7C(MR16 new)(6000K)', 3400),
(35, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-MR-18V-2835-7A-1.0 (MR16)(3000k)', 3500),
(36, 'LIGHT_SOURCE-(SKD)-(Store3)', 'SKD-MR-18V-2835-7A-1.0 (MR16)(6000k)', 3600),
(37, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L37DHA-V0 (3W)', 3700),
(38, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L37DHA-V0 (3W)', 3800),
(39, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L37DHA-V0 (5W)', 3900),
(40, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L37DHA-V0 (5W)', 4000),
(41, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L37DHA-V0 (MR16)', 4100),
(42, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L37DHA-V0 (MR16)', 4200),
(43, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-DHA-V0 (MR16)(OLD)', 4300),
(44, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L60DHC-V3 (7W)', 4400),
(45, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L60DHC-V3 (7W)', 4500),
(46, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-B60DHB-V1(0211-00297)(9W new)', 4600),
(47, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-B60DHB-V1(0211-00297)(9W new)', 4700),
(48, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-B60DHA-V1(9WC)', 4800),
(49, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-B60DHA-V1(9WP)', 4900),
(50, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-B60DHA-V1(9WC)', 5000),
(51, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-B60DHA-V1(9WP)', 5100),
(52, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-BZPDHC-V0(0211-00299)(15W new)', 5200),
(53, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-BZPDHC-V0(0211-00299)(15W new)', 5300),
(54, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L70DHA-V0 (12WP)', 5400),
(55, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L70DHA-V0 (12WP)', 5500),
(56, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L70DHA-V0(15WP)', 5600),
(57, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-L70DHA-V0(15WC)', 5700),
(58, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L70DHA-V0(15WP)', 5800),
(59, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-L70DHA-V0(15WC)', 5900),
(60, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-BZPDHA-V0 (0211-00176)(20W)', 6000),
(61, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-BZPDHA-V0 (0211-00176)(20W)', 6100),
(62, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JTX-JZDHA-V0 (40W)', 6200),
(63, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JTX-JZDHA-V0 (40W)', 6300),
(64, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JT-L27DH-EMC-V1 (Filament)(10W)', 6400),
(65, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JT-L27DH-EMC-V1(Filament)(10W)', 6500),
(66, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JT-L27DH-EMC-V1 (Filament)(8W)', 6600),
(67, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JT-L27DH-EMC-V1(Filament)(8W)', 6700),
(68, 'DRIVER(SMT/SKD)-(Store3)', 'SMT-JT-M14-V1 (Filament)(4W)', 6800),
(69, 'DRIVER(SMT/SKD)-(Store3)', 'SKD-JT-M14-V1 (Filament)(4W)', 6900),
(70, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-G45-18V2835-7A-1.1 (G45)(3/5W)', 7000),
(71, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-C35-18V2835-7A-1.0 (C35 new)', 7100),
(72, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-LED-C37-2835-9V-8-1.0 (5W Candle)', 7200),
(73, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A60-9V2835-10E-1.5 (7W)', 7300),
(74, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-A60-9V2835-20G-1.0 (00211-0325) (9W new)', 7400),
(75, 'LIGHT_SOURCE-(CKD)-(Store3)', '(0211-0015)CKD-JTX-V4-A60-10A-1.0 (9W Ceramic)', 7500),
(76, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A60-9V2835-10A-1.1(9W Plastic)', 7600),
(77, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A60-9V2835-20E-V1.0 (9W A70)', 7700),
(78, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A60-9V2835-20B-1.0(12W Ceramic)', 7800),
(79, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A60-9V2835-20A-1.2(12W Plastic)', 7900),
(80, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-A70-9V2835-27A-1.0 (15W new)', 8000),
(81, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A70-9V2835-26A-1.1(15W Ceramic)', 8100),
(82, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-A70-9V2835-26B-1.1 (15W Plastic)', 8200),
(83, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-T20-9V2835-40A-1.2(0211-00110) (20W)', 8300),
(84, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-JTX-T40-9V2835-84A-1.1 (40W)', 8400),
(85, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-Driver+LED (3W-DL)(1603-00381)(6000K)', 8500),
(86, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-Driver+LED (3W-DL)(1603-00380)(3000K)', 8600),
(87, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-TB-3V-2835-36A-1.0-1.0(6W-DL)(1504-00408)(3000K)', 8700),
(88, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-TB-3V-2835-36A-1.0-1.0(6W-DL)(1504-00407)(6000K)', 8800),
(89, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-LED Board (18W-DL) (1501-00409)(3000K)', 8900),
(90, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-LED Board (18W-DL) (1501-00410)(6000K)', 9000),
(91, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-T8-3V2835-48A-1.0(Tube 60cm)(1501-00334)(6000K)', 9100),
(92, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-T8-3V2835-48A-1.0(Tube 60cm)(1501-00334)(3000K)', 9200),
(93, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-T8-3V3835-96A-1.0(Tube 120cm)(1501-00333)(6000K)', 9300),
(94, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-T8-3V3835-96A-1.0(Tube 120cm)(1501-00333)(3000K)', 9400),
(95, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-BEAD Board 2835-12B10c(1501-000405)(60*60)(3000K)', 9500),
(96, 'LIGHT_SOURCE-(CKD)-(Store3)', 'SKD-BEAD Board 2835-12B10c(1501-000406)(60*60)(6000K)', 9600),
(97, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-MR16-7C(MR16 new)', 9700),
(98, 'LIGHT_SOURCE-(CKD)-(Store3)', 'CKD-MR-18V-2835-7A-1.0 (MR16)', 9800),
(99, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-L37DHA-V0(C35/G45/MR16)', 9900),
(100, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-DHA-V0 (MR16)(OLD)', 10000),
(101, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-L60DHC-V3 (7W)', 10100),
(102, 'DRIVER(CKD)-(Store3)', 'CKD-B60DHB-V1(0211-00297)(9W new)', 10200),
(103, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-B60DHA-V1(9W)', 10300),
(104, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-BZPDHC-V0(0211-00299)(15W new)', 10400),
(105, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-L70DHA-V0(12W-15W)', 10500),
(106, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-BZPDHA-V0 (0211-00176)(20W)', 10600),
(107, 'DRIVER(CKD)-(Store3)', 'CKD-JTX-JZDHA-V0 (40W)', 10700),
(108, 'DRIVER(CKD)-(Store3)', 'SKD-BT8DHA-V1-6W-DL (1603-00389)', 10800),
(109, 'DRIVER(CKD)-(Store3)', 'SKD-BT8DHA-V1-18W-DL (1603-00387)', 10900),
(110, 'DRIVER(CKD)-(Store3)', 'SKD-Drive tupe (85-265/230MA)', 11000),
(111, 'DRIVER(CKD)-(Store3)', 'SKD-B10C-5W-E12-2700K-T(48W-600MA)(60*60)', 11100),
(112, 'DRIVER(CKD)-(Store3)', 'CKD-JT-L27DH-EMC-V1(Filament)', 11200),
(113, 'DRIVER(CKD)-(Store3)', 'CKD-JT-M14-V1 (Filament)', 11300),
(114, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00018 (yellow)', 11400),
(115, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00016 (yellow)', 11500),
(116, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00006(yellow)', 11600),
(117, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00008(white)', 11700),
(118, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00020(white)', 11800),
(119, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1206-00021(white)', 11900),
(120, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1202-00034 (white)', 12000),
(121, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'LED Source/1202-00033 (yellow)', 12100),
(122, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'filament light source (C35)', 12200),
(123, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'filament light source (T35)', 12300),
(124, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'filament light source (G45)', 12400),
(125, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'filament light source (A70)', 12500),
(126, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'filament light source (A60)', 12600),
(127, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'bridge rectifier (0210-00003)(C35/ G45/7W/ MR16)', 12700),
(128, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'bridge rectifier (0210-0007)(20W/15W/9W)', 12800),
(129, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'bridge rectifier(0210-0002) /(8W/10W)', 12900),
(130, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'bridge rectifier/(1102-00028)(20W/15W/40W)', 13000),
(131, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', '(0210-00009)', 13100),
(132, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'DIODE (0209-00001) SMA-', 13200),
(133, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip diode(0201-00094)/(20W/15W/9W/40W/7W/MR16/C35/G45)', 13300),
(134, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip diode(0201-00003)/', 13400),
(135, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Diode(0209-0008)/(G45/ C35)', 13500),
(136, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor(0203-00013)(100NF)/(G45/C35/8W/10W)', 13600),
(137, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor(0203-0006)/(G45/ A60/ MR16/C35)', 13700),
(138, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor/', 13800),
(139, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor(0203-0009)/(40W/8W/10W/15W/9W/20W)', 13900),
(140, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor/(0203-000052)(G45/C35)', 14000),
(141, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor/(0203-00052) (9W)', 14100),
(142, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor/2012-(0203-00052)', 14200),
(143, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip capacitor/(0203-00044)(15W/20W/9W/40W)', 14300),
(144, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip inductor(0205-00015)/(G45/C35)', 14400),
(145, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'IC (1102-00011)', 14500),
(146, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'IC/ (1102-00013)(C35/G45/7W/MR16/8W/10 W)', 14600),
(147, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'IC/(1102-00029)(40W)', 14700),
(148, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'IC/(1102-00042) (9W)', 14800),
(149, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'IC-SM-SM7313C-(G45/C35)', 14900),
(150, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/1206-(0201-00057)(C35/G45/7W/9W )', 15000),
(151, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/0805-(0201-00011)', 15100),
(152, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Chip resistor/(0201-0322)', 15200),
(153, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Chip resistor/(0201-0262)', 15300),
(154, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00023)0805(20W/40W)', 15400),
(155, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00024)(15W)', 15500),
(156, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-000235)(15W)', 15600),
(157, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'CHIP RESISTOR (0201-00027) 0805-', 15700),
(158, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00032)(7W)', 15800),
(159, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00033)0805-', 15900),
(160, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/ (0201-00040)(MR16/9W)', 16000),
(161, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor(0201-00093)/(8W/10W)', 16100),
(162, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor(0201-00148)/', 16200),
(163, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'CHIP RESISTOR (0201-00047) 0805-', 16300),
(164, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Chip Resistor(0201-00096)', 16400),
(165, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00053) (G45)', 16500),
(166, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'CHIP RESISTOR (0201-00056) 0805-', 16600),
(167, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor(0204-00341)/(C35)', 16700),
(168, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00107)(C35)', 16800),
(169, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor (0201-00110)', 16900),
(170, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/', 17000),
(171, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-0012)(MR16)', 17100),
(172, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00285)(9W)', 17200),
(173, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00258)', 17300),
(174, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/ (0201-00037)(C35 /G45/ 7W / MR16)', 17400),
(175, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00045)(20W/40W/15W)', 17500),
(176, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/1206-(0201-00061)(20W/40W/15W)', 17600),
(177, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/1206-(0201-00060)', 17700),
(178, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Chip Resistor(0201-000179)', 17800),
(179, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'Chip Resistor(0201-00043)0805-', 17900),
(180, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00235)(9W)', 18000),
(181, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-0013)0805-(20W/40W/15W/9W)', 18100),
(182, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-0013)0805F-(20W/40W/15W/9W)', 18200),
(183, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00256)0805-(20W/40W/15W)', 18300),
(184, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0210-00072) (G45)', 18400),
(185, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00082)(15W/G45/C35)', 18500),
(186, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00142)1206-(20W/40W)', 18600),
(187, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-00083)1206-', 18700),
(188, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-000103)(7W/C35)', 18800),
(189, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/(0201-000233)', 18900),
(190, 'ELECTRICAL_COMPONENTS_ROLES-(Store3)', 'chip resistor/', 19000),
(191, 'ELECTRICAL_COMPONENTS-(store3)', 'TRANSFORMER (0206-00005)', 19100),
(192, 'ELECTRICAL_COMPONENTS-(store3)', 'TRANSFORMER (0206-00006)', 19200),
(193, 'ELECTRICAL_COMPONENTS-(store3)', 'TRANSFORMER (0206-00007)', 19300),
(194, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer /(0206-00067)(C35/ MR16/7W)', 19400),
(195, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer/(0206-00019)(G45)', 19500),
(196, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer(0206-00058)(40W)', 19600),
(197, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer (0206-00053)(20W/15W/9W)', 19700),
(198, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer (0206-00063)', 19800),
(199, 'ELECTRICAL_COMPONENTS-(store3)', 'Transformer(0206-0004) /(8W/10W)', 19900),
(200, 'ELECTRICAL_COMPONENTS-(store3)', 'CAPACITOR (0204-00008),400V-6.3*12 (C35/ G45/7W )', 20000),
(201, 'ELECTRICAL_COMPONENTS-(store3)', 'CAPACITOR (0204-00011),400V-6.3*9 (MR16)', 20100),
(202, 'ELECTRICAL_COMPONENTS-(store3)', 'CAPACITOR (0204-00008),400V-6.3*11', 20200),
(203, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor//Φ6.3*9 (G45)', 20300),
(204, 'ELECTRICAL_COMPONENTS-(store3)', 'CAPACITOR (0204-00022),400V-8*12', 20400),
(205, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor //Φ5*11(20W/40W/15W)', 20500),
(206, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor//Φ10*13mm (9W)', 20600),
(207, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor//Φ12.5*20/ (0204-0020)(40W)', 20700),
(208, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor//Φ10*16mm(0204-00174) (20W/15W)', 20800),
(209, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor//Φ12.5*20mm(0204-0093)', 20900),
(210, 'ELECTRICAL_COMPONENTS-(store3)', 'piezo resistor /ZOV-07D', 21000),
(211, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor /(0.1UF)(104)(0204-00086)', 21100),
(212, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor / CBB21(47UF)(473)(0204-00171)', 21200),
(213, 'ELECTRICAL_COMPONENTS-(store3)', 'Capacitor /(224)', 21300),
(214, 'ELECTRICAL_COMPONENTS-(store3)', 'INDUCTOR (0205-00003)', 21400),
(215, 'ELECTRICAL_COMPONENTS-(store3)', 'Inductor/(0205-00008)(C35/G45/7W/ MR16)', 21500),
(216, 'ELECTRICAL_COMPONENTS-(store3)', 'Inductor/(0205-00010)(8W/10W)', 21600),
(217, 'ELECTRICAL_COMPONENTS-(store3)', 'Resistor (0202-0046)', 21700),
(218, 'ELECTRICAL_COMPONENTS-(store3)', 'Resistor (0202-0009)', 21800),
(219, 'ELECTRICAL_COMPONENTS-(store3)', 'Resistor (0202-00036)', 21900),
(220, 'ELECTRICAL_COMPONENTS-(store3)', 'Resistor (0202-00017)', 22000),
(221, 'ELECTRICAL_COMPONENTS-(store3)', 'Resistor (0202-00072)', 22100),
(222, 'ELECTRICAL_COMPONENTS-(store3)', 'fuze/2A/250V (0302-0003)(40W)', 22200),
(223, 'ELECTRICAL_COMPONENTS-(store3)', 'Transistor-5N65KL-MTQ(54UEC4)(TO-220FYGDC)', 22300),
(224, 'PARTS-(Store1)', 'cup(0403-00207) (G45 3W)', 22400),
(225, 'PARTS-(Store1)', 'cover(0402-00107) (G45 3W)', 22500),
(226, 'PARTS-(Store1)', '(0403-00165) CUP-CANDLE', 22600),
(227, 'PARTS-(Store1)', '(0402-00104)COVER-CANDLE', 22700),
(228, 'PARTS-(Store1)', '(0402-00129)COVER-TIP', 22800),
(229, 'PARTS-(Store1)', 'AL-BASE-white-E14(دوايه Candle)', 22900),
(230, 'PARTS-(Store1)', 'AL-BASE-black-E14(دوايه Candle)', 23000),
(231, 'PARTS-(Store1)', 'COVER-A60-9W-C', 23100),
(232, 'PARTS-(Store1)', 'CUP-A60-7/9W-C', 23200),
(233, 'PARTS-(Store1)', '(0404-0002)COVER-A60-49.5MM (9/12W-P)', 23300),
(234, 'PARTS-(Store1)', '(0403-00092)CUP-A60-9W-P', 23400),
(235, 'PARTS-(Store1)', 'Heat SINK-9W-P (منفث حرارى )', 23500),
(236, 'PARTS-(Store1)', 'CUP-A60-12W-P', 23600),
(237, 'PARTS-(Store1)', 'Heat SINK-A60-12W-P (منفث حرارى )', 23700),
(238, 'PARTS-(Store1)', '(0404-00041)COVER-A70-(9W-A70/15W)', 23800),
(239, 'PARTS-(Store1)', '(0403-00107)CUP-A70-9W-C', 23900),
(240, 'PARTS-(Store1)', '(0403-00107)BASE-A70-9W-C', 24000),
(241, 'PARTS-(Store1)', '(0403-00131)CUP-A70-15W-P', 24100),
(242, 'PARTS-(Store1)', 'CUP-A70-15W-C', 24200),
(243, 'PARTS-(Store1)', 'BASE-A70-15W-C', 24300),
(244, 'PARTS-(Store1)', 'Heat SINK-A70-15ًW(منفث حرارى )', 24400),
(245, 'PARTS-(Store1)', 'AL-BASE-white-E27(دوايه)', 24500),
(246, 'PARTS-(Store1)', 'AL-BASE-black-E27(دوايه)', 24600),
(247, 'PARTS-(Store1)', 'cup T80 (0403-00048)(20W)', 24700),
(248, 'PARTS-(Store1)', 'PC COVER T80(0404-00021)(20W)', 24800),
(249, 'PARTS-(Store1)', 'base/T80-E27 (0403-0052)(20W)', 24900),
(250, 'PARTS-(Store1)', 'cup T120(0403-00050)(40W)', 25000),
(251, 'PARTS-(Store1)', 'cover T120(0404-00023) (40W)', 25100),
(252, 'PARTS-(Store1)', 'base T120-E27 (0403-0054)(40W)', 25200),
(253, 'PARTS-(Store1)', 'Lamp Holder (0409-00171)', 25300),
(254, 'PARTS-(Store1)', 'DL-Cup Ring (3W)(0403-00220)', 25400),
(255, 'PARTS-(Store1)', 'DL-Back cover(3W)(0403-000221)', 25500),
(256, 'PARTS-(Store1)', 'DL-Hoder (3W)(0403-000222)', 25600),
(257, 'PARTS-(Store1)', 'DL-Lense(3W)(0403-00223)', 25700),
(258, 'PARTS-(Store1)', 'DL-wire(3W)(0303-00021)', 25800),
(259, 'PARTS-(Store1)', 'DL-Cup Ring(6W)(0403-00224)', 25900),
(260, 'PARTS-(Store1)', 'DL-Lense(6W)(0403-00227)', 26000),
(261, 'PARTS-(Store1)', 'DL-Back cover with reflector light cup(6W)(0403-00226/225)', 26100),
(262, 'PARTS-(Store1)', 'Driver cover(6-18W)(0403-00228)', 26200),
(263, 'PARTS-(Store1)', 'DL-Cup Ring(18W)(0403-00229)', 26300),
(264, 'PARTS-(Store1)', 'DL-Back cover with reflector light cup(18W)(0403-00230/231)', 26400),
(265, 'PARTS-(Store1)', 'DL-Lense(18W)(0403-00232)', 26500),
(266, 'PARTS-(Store1)', 'splats needle(0403-00120)', 26600),
(267, 'PARTS-(Store1)', 'driver wire (0303-0005)', 26700),
(268, 'PARTS-(Store1)', 'plug in wire (0303-0006)', 26800),
(269, 'PARTS-(Store1)', 'PC COVER TUBE 60 CM', 26900),
(270, 'PARTS-(Store1)', 'PC COVER TUBE 120 CM', 27000),
(271, 'PARTS-(Store1)', 'T8 (دوايه Tube)', 27100),
(272, 'PARTS-(Store1)', 'Frame 60*60', 27200),
(273, 'PARTS-(Store1)', 'Driver plastic cover 60*60', 27300),
(274, 'PARTS-(Store1)', 'FOAM 60*60', 27400),
(275, 'PARTS-(Store1)', '(Back cover)صاج ظهر كشاف 60*60', 27500),
(276, 'PARTS-(Store1)', 'عاكس 60*60(reflector sheet)', 27600),
(277, 'PARTS-(Store1)', 'بالت موجه 60*60(Crystal sheet)', 27700),
(278, 'PARTS-(Store1)', 'بالت شفاف 60*60(up Cover)', 27800),
(279, 'PARTS-(Store1)', 'سلك كشاف جالك 60*60', 27900),
(280, 'PARTS-(Store1)', '(0403-00010) CUP (MR16 5W/GU10)', 28000),
(281, 'PARTS-(Store1)', 'Base (0403-00028)(MR16 5W )', 28100),
(282, 'PARTS-(Store1)', 'LENSE (0405-0003)(MR16 5W)', 28200),
(283, 'PARTS-(Store1)', 'aluminum substrate(JTMR16-7C ) (منقث حرارى )(MR16)', 28300),
(284, 'PARTS-(Store1)', 'Jumb Ring(0601-00056)', 28400),
(285, 'PARTS-(Store1)', 'Jumb Ring(0601-00053)', 28500),
(286, 'PARTS-(Store1)', 'E27 to GU10 converter', 28600),
(287, 'PARTS-(Store1)', 'E27 to E14 converter', 28700),
(288, 'Carton-(Roof)', 'BOX (15W /12W) (خارجيه)', 28800),
(289, 'Carton-(Roof)', 'BOX (9W)(خارجيه)', 28900),
(290, 'Carton-(Roof)', 'BOX (candle)(خارجيه)', 29000),
(291, 'Carton-(Roof)', 'BOX (5W TIP)(خارجيه)', 29100),
(292, 'Carton-(Roof)', 'BOX (5W+ 3W)(خارجيه)', 29200),
(293, 'Carton-(Roof)', 'BOX (7W)(خارجيه)', 29300),
(294, 'Carton-(Roof)', 'BOX (Panel 60*60) (خارجيه)', 29400),
(295, 'Carton-(Roof)', 'BOX Tube 60cm (خارجى)', 29500),
(296, 'Carton-(Roof)', 'BOX Tube 120cm (خارجى)', 29600),
(297, 'Carton-(Roof)', 'lamp box 3W / White', 29700),
(298, 'Carton-(Roof)', 'lamp box 3W /Yellow', 29800),
(299, 'Carton-(Roof)', 'lamp box 5W / White', 29900),
(300, 'Carton-(Roof)', 'lamp box 5W /Yellow', 30000),
(301, 'Carton-(Roof)', 'lamp box 7W / White', 30100),
(302, 'Carton-(Roof)', 'lamp box 7W /Yellow', 30200),
(303, 'Carton-(Roof)', 'lamp box 9W', 30300),
(304, 'Carton-(Roof)', 'lamp box 12W', 30400),
(305, 'Carton-(Roof)', 'lamp box 15 W', 30500),
(306, 'Carton-(Roof)', 'lamp box 15 W / white', 30600),
(307, 'Carton-(Roof)', 'lamp box 15 W / Yellow', 30700),
(308, 'Carton-(Roof)', 'lamp box candle White', 30800),
(309, 'Carton-(Roof)', 'lamp box candle Yellow', 30900),
(310, 'Carton-(Roof)', 'lamp box candle', 31000),
(311, 'Carton-(Roof)', 'lamp box tip White', 31100),
(312, 'Carton-(Roof)', 'lamp box tip Yellow', 31200),
(313, 'Carton-(Roof)', 'lamp box tip', 31300),
(314, 'Carton-(Roof)', 'lamp box 40 W', 31400),
(315, 'Carton-(Roof)', 'lamp box 20 W', 31500),
(316, 'Carton-(Roof)', 'lamp box MR16', 31600),
(317, 'Carton-(Roof)', 'lamp box 9W old', 31700),
(318, 'Carton-(Roof)', 'lamp box DL 18W White', 31800),
(319, 'Carton-(Roof)', 'lamp box DL 18W Yellow', 31900),
(320, 'Carton-(Roof)', 'lamp box DL 6W White', 32000),
(321, 'Carton-(Roof)', 'lamp box DL 6W Yellow', 32100),
(322, 'Carton-(Roof)', 'lamp box DL 3W White', 32200),
(323, 'Carton-(Roof)', 'lamp box DL 3W Yellow', 32300),
(324, 'Carton-(Roof)', 'lamp box Tube 60cm White', 32400),
(325, 'Carton-(Roof)', 'lamp box Tube 60cm Yellow', 32500),
(326, 'Carton-(Roof)', 'lamp box Tube 120cm White', 32600),
(327, 'Carton-(Roof)', 'lamp box Tube 120cm Yellow', 32700),
(328, 'Manufacture_material-(Roof)', 'لوح قصدير', 32800),
(329, 'Manufacture_material-(Roof)', 'بكره قصدير (TIN)', 32900),
(330, 'Manufacture_material-(Roof)', '817 glue', 33000),
(331, 'Manufacture_material-(Roof)', 'SElICON Cartrige gray', 33100),
(332, 'Manufacture_material-(Roof)', 'SElICON Cartrige white', 33200),
(333, 'Manufacture_material-(Roof)', 'SELICOM 750', 33300),
(334, 'Manufacture_material-(Roof)', 'HEAT SELICON RED', 33400),
(335, 'Manufacture_material-(Roof)', 'Selicon white (Heat Conduction)', 33500),
(336, 'Manufacture_material-(Roof)', 'solitep', 33600),
(337, 'Manufacture_material-(Roof)', 'Double Face(27*20) sponge pastie(0604-00021)', 33700),
(338, 'Manufacture_material-(Roof)', 'Double Face', 33800),
(339, 'Manufacture_material-(Roof)', 'sebrto bottle', 33900);

-- --------------------------------------------------------

--
-- Table structure for table `rm_trs`
--

DROP TABLE IF EXISTS `rm_trs`;
CREATE TABLE IF NOT EXISTS `rm_trs` (
  `RM_TRS_ID` int(100) NOT NULL AUTO_INCREMENT COMMENT 'the ID of the row',
  `Form_number` int(100) DEFAULT NULL COMMENT 'the Request Form Number',
  `dateRequest` date DEFAULT NULL COMMENT 'Request Date',
  `DateApprove` date DEFAULT NULL COMMENT 'Appover Date',
  `RawMaterial_ID` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Raw Material ID number',
  `QuantityOUT` int(100) DEFAULT NULL COMMENT 'Quantity add to stor',
  `QuantityIN` int(100) DEFAULT NULL COMMENT 'Quantity out from stor',
  PRIMARY KEY (`RM_TRS_ID`),
  KEY `Form Number` (`Form_number`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rm_trs`
--

INSERT INTO `rm_trs` (`RM_TRS_ID`, `Form_number`, `dateRequest`, `DateApprove`, `RawMaterial_ID`, `QuantityOUT`, `QuantityIN`) VALUES
(1, 1, '2019-05-14', '2019-05-21', '1', 10, 5),
(2, 1, '2019-05-14', '2019-05-21', '38', 10, 5),
(3, 1, '2019-05-14', '2019-05-21', '224', 10, 5),
(4, 1, '2019-05-14', '2019-05-21', '225', 10, 5),
(5, 1, '2019-05-14', '2019-05-21', '245', 10, 5),
(6, 1, '2019-05-14', '2019-05-21', '292', 10, 5),
(8, 2, '2019-05-14', '2019-05-22', '1', 10, 0),
(9, 2, '2019-05-14', '2019-05-22', '150', 10, 0),
(10, 2, '2019-05-14', '2019-05-22', '70', 10, 0),
(12, 3, '2019-05-14', NULL, '1', 0, 0),
(13, 4, '2019-05-14', NULL, '1', 5, 5),
(14, 5, '2019-05-14', '2019-05-21', '1', 5, 5),
(16, 6, '2019-05-21', NULL, '1', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'user ID',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'user Email address ',
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user password',
  `reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'time of the registration',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='users table';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `password`, `reg_time`) VALUES
(1, 'test@test.com', '$2y$10$EWO5ZQo8JrtrUDkModUIQe7QAoGcc29xHxS7sQb4b0BHuIA1bQ5ue', '2019-04-04 15:03:00'),
(2, 'test2@test.com', '$2y$10$c7r9U5EXJrkHDkXLSnhjQ.TutKllpMVdLl32QjTrPzs0an9qaQRyK', '2019-04-08 09:58:55');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
