<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once 'inc/config.php';
  ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login / Register </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- UIkit CSS -->
        <link rel="stylesheet" href="tool/css/uikit.min.css" />
		
   </head>
   
   <body>
    <div class="uk-section uk-container uk-center">
    <?php 
        if (date("A")=="AM") { //day or night
          echo "Good Morning.". " <br /> Today is: ".date("l jS \of F Y h:i:s A");
        }else {
          echo "Good Night.". " <br /> Today is: ".date("l jS \of F Y h:i:s A");
        }
    ?>
    <br />
    <a href="/login.php"> Login </a>
     <a href="/Register.php"> Register </a>
    

   <?php 
        require_once 'inc/footer.php'
    ?>    
		
      	
  </body>
</html>