<?php 
//if the CONFIG varaible is not defined it will exit with message
if(!defined('_CONFIG_')){
	exit("there is no config file");
}
//our config is below. It is required from login and register
if (!isset($_SESSION)){
	session_start();
}

include_once "classes/Filter.php"; //to include filter to check if the email is good
include_once "classes/DB.php";  // to include the database classes (oop)
include_once "functions.php";  // to include the functions
include_once "classes/user.php";  // to include the user class (OOP)
include_once "classes/pages.php";  // to include the user class (OOP)
$con = DB::getConnection()   //to get connect to the database call the public static function from the classes DB
?>