<?php
	if(!defined('_CONFIG_')){
			exit("there is no config file");
		}//this for security inorder that no one can run that without the index
 /**
  * 
  */
 class user
 {        // the only different between the 2 functions is one its varaible is integer and the other is string!!
 	 public static function findUserByID($user_id){
      		$user_id = (int)Filter::int($user_id);
      		$con = DB::getConnection();
            $getUserInfo = $con->prepare("SELECT * FROM user WHERE user_id = :user_id LIMIT 1"); 
            $getUserInfo ->bindParam(':user_id', $user_id, PDO::PARAM_STR);
            $getUserInfo->execute();
            $User =  $getUserInfo->fetch(PDO::FETCH_ASSOC);
            return $User;
           }

	 public static function findUserByEmail($email) {
	      $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		  $email = (string)Filter::String($email);    
	      $findUser = $con->prepare("SELECT user_id, password FROM user WHERE email = LOWER(:email) LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	      $findUser ->bindParam(':email', $email, PDO::PARAM_STR);
	      $findUser->execute();
	      $User_found =  $findUser->fetch(PDO::FETCH_ASSOC);// this return one row only
	      return $User_found; 
		}

	public static function FPList(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$FPList = $con->prepare("SELECT * FROM fp_list"); //find all finish product list
		
		$FPList->execute();
		
		return $FPList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}

	public static function findFP($bulbT){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findFP = $con->prepare("SELECT * FROM fp_list WHERE FP_ID = :bulbT LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findFP ->bindParam(':bulbT', $bulbT, PDO::PARAM_INT);
	    $findFP->execute();
	    $Bulb_found =  $findFP->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $Bulb_found; 
	}

	public static function CustList(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$CustList = $con->prepare("SELECT * FROM cust_list"); //find all finish product list
		
		$CustList->execute();
		
		return $CustList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}

	public static function findLastRecord($tabName, $idName){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		//$tabName = (string)Filter::String($tabName);
		$findLR = $con->prepare("SELECT * FROM $tabName getLastRecord ORDER BY  	$idName DESC LIMIT 1"); //we search for last row in the table. they use that techinic because -	SELECT LAST_INSERT_ID()- if you insert multiple rows into a table using a single INSERT statement, the LAST_INSERT_ID function returns the last insert id of the first row
		
	    $findLR->execute();
	    $LR_found =  $findLR->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $LR_found; //return last row
	}

	public static function findCustByName($custName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findCust = $con->prepare("SELECT * FROM cust_list WHERE Cust_ID = :CustomerID LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findCust ->bindParam(':CustomerID', $custName, PDO::PARAM_INT);
	    $findCust->execute();
	    $Cust_found =  $findCust->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $Cust_found; //array of customer detail 
		
	}	
	
	public static function SumTransFP($BulbT){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findSum = $con->prepare("SELECT (COALESCE(IniBalance.iniBal,0) - COALESCE(OFSBalance.OFSBal,0) + COALESCE(IFSBalance.IFSBal,0) +COALESCE(IFPBalance.IFPBal,0) ) as totalBulbBal FROM 
			(SELECT FP_ID , sum(InitialQuantity) as iniBal FROM fp_list WHERE FP_ID =:BulbT  GROUP BY FP_ID) as IniBalance 
			 
			LEFT JOIN
			(SELECT FP_OFS_ID, Item_desc, sum(Quantity) as OFSBal FROM  fp_ofs WHERE Item_desc = :BulbT GROUP BY Item_desc ) as OFSBalance
			ON IniBalance.FP_ID = OFSBalance.Item_desc 
			
			LEFT JOIN
			(SELECT FP_IFS_ID, Item_desc, sum(Quantity) as IFSBal FROM  fp_ifs WHERE Item_desc = :BulbT GROUP BY Item_desc ) as IFSBalance
			ON IniBalance.FP_ID = IFSBalance.Item_desc

			LEFT JOIN
			(SELECT FP_IFP_ID, Item_desc, sum(Quantity) as IFPBal FROM  fp_ifp WHERE Item_desc = :BulbT GROUP BY Item_desc ) as IFPBalance
		   ON IniBalance.FP_ID = IFPBalance.Item_desc "); 
	    $findSum ->bindParam(':BulbT', $BulbT, PDO::PARAM_STR);

	    $findSum->execute();
	    $Sum_found =  $findSum->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $Sum_found; //array of customer detail 
		
	}



	public static function FPBalace(){ //=Initial quantity - out to sale quantity + in from sale quantity + in from production quantity.
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findBalance = $con->prepare(
		"SELECT IniBalance.FP_ID,IniBalance.BulbType,(IniBalance.IniBal-COALESCE (OFSQuantity.OFSQuan,0) + COALESCE (IFSQuantity.IFSQuan,0) +COALESCE (IFPQuantity.IFPQuan,0)) as Balance FROM 
		(SELECT FP_ID, BulbType,sum(InitialQuantity) as IniBal FROM fp_list GROUP BY FP_ID) as IniBalance 
		LEFT JOIN 
		(SELECT  FP_trans_ID, Item_desc, sum(Quantity) as OFSQuan FROM fp_ofs WHERE COALESCE(Date_app,0) != 0 GROUP BY Item_desc ) as OFSQuantity 
		ON  OFSQuantity.Item_desc = IniBalance.FP_ID 
		LEFT JOIN 
		(SELECT  FP_trans_ID, Item_desc, sum(Quantity) as IFSQuan FROM fp_ifs WHERE COALESCE(Date_app,0) != 0 GROUP BY Item_desc ) as IFSQuantity 
		ON  IFSQuantity.Item_desc = IniBalance.FP_ID 
		LEFT JOIN 
		(SELECT  FP_trans_ID, Item_desc, sum(Quantity) as IFPQuan FROM fp_ifp WHERE COALESCE(Date_app,0) != 0 GROUP BY Item_desc ) as IFPQuantity 
		ON  IFPQuantity.Item_desc = IniBalance.FP_ID    
		ORDER BY IniBalance.FP_ID"); //
	    
	    $findBalance->execute(); 
	   // $Balance_found =  $findBalance->fetch(PDO::FETCH_ASSOC);// this return one row only in an array for Multi row send the holl fetch!!
	    return $findBalance; //array of customer detail 
		
	}

	public static function RMBalace(){ //=Initial quantity - out to sale quatity + in from sale quatity + in from production.
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findBalance = $con->prepare(
		"SELECT IniBalance.RM_ID,IniBalance.Groups,IniBalance.Raw_Material,(IniBalance.IniBal - COALESCE (RMTranQuan.RMOutTran,0)+ COALESCE (RMTranQuan.RMInTran,0)) as Balance FROM 
		(SELECT RM_ID, Groups,RAW_Material,sum(Initial_Quantity) as IniBal FROM rm_list GROUP BY RM_ID) as IniBalance 
		LEFT JOIN 
		(SELECT RM_TRS_ID, RawMaterial_ID, sum(QuantityOut) as RMOutTran,sum(QuantityIN) as RMInTran FROM rm_trs WHERE COALESCE(DateApprove,0) != 0 GROUP BY RawMaterial_ID ) as RMTranQuan 
		ON  RMTranQuan.RawMaterial_ID = IniBalance.RM_ID  
		ORDER BY IniBalance.RM_ID"); 
	    
	    $findBalance->execute(); 
	   // $Balance_found =  $findBalance->fetch(PDO::FETCH_ASSOC);// this return one row only in an array for Multi row send the holl fetch!!
	    return $findBalance; //array of customer detail 
		
	}

	public static function RMBalaceByGroup($SelGroup){ //=Initial quantity - out to sale quatity + in from sale quatity + in from production.
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findBalance = $con->prepare(
		"SELECT IniBalance.RM_ID,IniBalance.Groups,IniBalance.Raw_Material,(IniBalance.IniBal - COALESCE (RMTranQuan.RMOutTran,0)+ COALESCE (RMTranQuan.RMInTran,0)) as Balance FROM 
		(SELECT RM_ID, Groups,RAW_Material,sum(Initial_Quantity) as IniBal FROM rm_list WHERE Groups =:SelGroup  GROUP BY RM_ID) as IniBalance 
		LEFT JOIN 
		(SELECT RM_TRS_ID, RawMaterial_ID, sum(QuantityOut) as RMOutTran,sum(QuantityIN) as RMInTran FROM rm_trs WHERE COALESCE(DateApprove,0) != 0 GROUP BY RawMaterial_ID ) as RMTranQuan
		ON  RMTranQuan.RawMaterial_ID = IniBalance.RM_ID  
		ORDER BY IniBalance.RM_ID"); 
	    
	     $findBalance ->bindParam(':SelGroup', $SelGroup, PDO::PARAM_STR);
	    $findBalance->execute(); 
	   // $Balance_found =  $findBalance->fetch(PDO::FETCH_ASSOC);// this return one row only in an array for Multi row send the holl fetch!!
	    return $findBalance; //array of customer detail 
		
	}


	public static function FPnotAppoved($TableName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$CustList = $con->prepare("SELECT * FROM $TableName WHERE COALESCE(Date_app,0) = 0 GROUP BY  Form_number "); //find all finish product list
		
		$CustList->execute();
		
		return $CustList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}

	public static function FPOutNoReceipt(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$CustList = $con->prepare("SELECT fp_ofs.Form_number ,cust_list.CustomerName,fp_ofs.Date_app FROM fp_ofs
			INNER JOIN cust_list ON cust_list.Cust_ID = fp_ofs.Cust_name
		 WHERE COALESCE(fp_ofs.Date_receipt , 0) = 0 and COALESCE(fp_ofs.Date_app, 0)!= 0 GROUP BY  fp_ofs.Form_number "); //find all finish product list
		
		$CustList->execute();
		
		return $CustList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}

	public static function FPInNoReceipt(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$CustList = $con->prepare("SELECT fp_ifs.Form_number ,cust_list.CustomerName,fp_ifs.Date_app FROM fp_ifs
			INNER JOIN cust_list ON cust_list.Cust_ID = fp_ifs.Cust_name
		 WHERE COALESCE(fp_ifs.Date_receipt , 0) = 0 and COALESCE(fp_ifs.Date_app, 0)!= 0 GROUP BY  fp_ifs.Form_number "); //find all finish product list
		
		$CustList->execute();
		
		return $CustList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}

	public static function findCustByID($CustID){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findSum = $con->prepare("SELECT CustomerName  FROM cust_list WHERE Cust_ID = :CustID "); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findSum ->bindParam(':CustID', $CustID, PDO::PARAM_INT);
	    $findSum->execute();
	    $Cust_found =  $findSum->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $Cust_found; //array of customer detail 
		
	}
	public static function FindFormReq($FormNum,$tableName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findFormReqDet = $con->prepare("SELECT  *  FROM $tableName WHERE  	Form_number = :FormNum "); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findFormReqDet ->bindParam(':FormNum', $FormNum, PDO::PARAM_INT);
	    $findFormReqDet->execute();
	    // this return one row only in an array
	    return $findFormReqDet; //array of customer detail 
		
	}

	public static function RMGroup(){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findGrp = $con->prepare("SELECT Groups FROM rm_list GROUP BY Groups ORDER BY RM_ID"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findGrp->execute();
	    
	    return $findGrp; 
	}

	public static function findComFromGRP($GrpName){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findCom = $con->prepare("SELECT RM_ID , RAW_Material FROM rm_list WHERE Groups = :GrpName ORDER BY RM_ID"); //we search for the email (lower case)in the database and if found stop search (limit 1)
		$findCom ->bindParam(':GrpName', $GrpName, PDO::PARAM_STR);
	    $findCom->execute();

	    	    
	    return $findCom; 
	}
 
 	public static function findRM($RawM){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findRM = $con->prepare("SELECT * FROM rm_list WHERE RM_ID = :RawM LIMIT 1"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findRM ->bindParam(':RawM', $RawM, PDO::PARAM_INT);
	    $findRM->execute();
	    $RM_found =  $findRM->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $RM_found; 
	}

	public static function SumTransRM($RawM){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$findSum = $con->prepare("SELECT sum(QuantityOUT) as SumQuOUT , sum(QuantityIN)as SumQuIN FROM rm_trs WHERE RawMaterial_ID = :RawMaterial_ID "); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findSum ->bindParam(':RawMaterial_ID', $RawM, PDO::PARAM_STR);
	    $findSum->execute();
	    $Sum_found =  $findSum->fetch(PDO::FETCH_ASSOC);// this return one row only in an array
	    return $Sum_found; //array of customer detail 
		
	}	

	public static function RMByBT(){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findBT = $con->prepare("SELECT BulbType FROM rm_bsc_list  GROUP BY BulbType"); //we search for the email (lower case)in the database and if found stop search (limit 1)
	    $findBT->execute();
	    
	    return $findBT; 
	}

	public static function findBSCFromBT($SelBT){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findBSC = $con->prepare("SELECT RM_B_ID , Body_SKD_CKD FROM rm_bsc_list WHERE BulbType = :BulbType GROUP BY Body_SKD_CKD ORDER BY RM_B_ID"); //we search for the email (lower case)in the database and if found stop search (limit 1)
		$findBSC ->bindParam(':BulbType', $SelBT, PDO::PARAM_STR);
	    $findBSC->execute();

	    	    
	    return $findBSC; 
	}

	public static function findRMByBSC($SelBSC, $SelBT ){
        $con = DB::getConnection(); //to connect to the public function getConnect() in DB
		
		$findRM = $con->prepare("SELECT RM_ID , RAW_Material , Goups FROM rm_bsc_list WHERE BulbType = :BulbType AND Body_SKD_CKD = :Body_SKD_CKD ORDER BY RM_B_ID"); //we search for the email (lower case)in the database and if found stop search (limit 1)
		$findRM ->bindParam(':BulbType', $SelBT, PDO::PARAM_STR);
		$findRM ->bindParam(':Body_SKD_CKD', $SelBSC, PDO::PARAM_STR);
	    $findRM->execute();

	    	    
	    return $findRM; 
	}

	public static function RMnotAppoved(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$ReqList = $con->prepare("SELECT * FROM rm_trs WHERE COALESCE( 	DateApprove ,0) = 0 GROUP BY  Form_number "); //find all Raw Material list not approved
		
		$ReqList->execute();
		
		return $ReqList; // by that way we return that index in order to get the holl column. the upper way fitch and return only one row
	}
 
 	public static function CustBalaceByNameList($custName,$tableName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$FindCustTranList = $con->prepare("SELECT TblName.Form_number, TblName.Date_receipt, TblName.Item_desc, FPList.BulbType, TblName.Quantity, TblName.Price 
			FROM 
			    (SELECT Cust_name, Form_number, Date_receipt, Item_desc, Quantity, Price FROM $tableName WHERE COALESCE(Date_receipt ,0) != 0 AND Cust_name = :Cust_name ORDER BY FP_trans_ID ) AS TblName
			LEFT JOIN 
			    (SELECT FP_ID, BulbType FROM fp_list) AS FPList
			ON FPList.FP_ID = TblName.Item_desc
			"); 
		$FindCustTranList ->bindParam(':Cust_name', $custName, PDO::PARAM_STR);
		
		$FindCustTranList->execute();
		
		return $FindCustTranList; 
	}

	public static function CustCashIN($custName,$tableName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$FindCustCashIN = $con->prepare("SELECT Cust_Cash_ID,  	Customer_ID, dateRequest, Amount
			FROM $tableName WHERE  Customer_ID = :Cust_name  "); 
		$FindCustCashIN ->bindParam(':Cust_name', $custName, PDO::PARAM_STR);
		
		$FindCustCashIN->execute();
		
		return $FindCustCashIN; 
	}


	public static function CustBalaceByNameTotal($custName){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$FindCustTran = $con->prepare("SELECT (COALESCE (CustIniBal.InitBal,0)+COALESCE(FP_IFS.IFS_SUM,0)- COALESCE(FP_OFS.OFS_SUM,0)+ COALESCE(CUST_CASH_IN.TotalCachIN,0)) as TotCustBal FROM
		    (SELECT Cust_ID, COALESCE(sum(InitBalance),0) as InitBal FROM cust_list WHERE Cust_ID = :Cust_name ) as CustIniBal
		    LEFT JOIN 
			(SELECT Price, Cust_name, COALESCE(sum(Price),0) as OFS_SUM   FROM fp_ofs WHERE COALESCE(Date_receipt ,0) != 0 AND Cust_name = :Cust_name ) as FP_OFS 
			ON CustIniBal.Cust_ID =  FP_OFS.Cust_name
			LEFT JOIN 
			(SELECT Price, Cust_name , COALESCE(sum(Price),0) as IFS_SUM  FROM fp_ifs WHERE COALESCE(Date_receipt ,0) != 0 AND Cust_name = :Cust_name ) as FP_IFS
		    ON CustIniBal.Cust_ID = FP_IFS.Cust_name 
		    LEFT JOIN
		    (SELECT Cust_Cash_ID,Customer_ID, dateRequest, sum(Amount) as TotalCachIN FROM customer_cash_in WHERE Customer_ID = :Cust_name) as CUST_CASH_IN 
		    ON CustIniBal.Cust_ID = CUST_CASH_IN.Customer_ID
		    ");

		$FindCustTran ->bindParam(':Cust_name', $custName, PDO::PARAM_STR);
		
		$FindCustTran->execute();
		$Sum_found =  $FindCustTran->fetch(PDO::FETCH_ASSOC);
		return $Sum_found; 
	}
	
	public static function CustListBalace(){
		$con = DB::getConnection(); //to call public function getconnection in DB to connect to database
		$FindCustTran = $con->prepare("SELECT  CustIniBal.Cust_ID,CustIniBal.CustomerName,(COALESCE (CustIniBal.InitBal,0)+COALESCE(FP_IFS.IFS_SUM,0)- COALESCE(FP_OFS.OFS_SUM,0)+COALESCE(CUST_CASH_IN.TotalCachIN,0)) as TotCustBal FROM
		    (SELECT Cust_ID,CustomerName, COALESCE(sum(InitBalance),0) as InitBal FROM cust_list GROUP BY Cust_ID   ) as CustIniBal
		    LEFT JOIN 
			(SELECT Price, Cust_name, COALESCE(sum(Price),0) as OFS_SUM   FROM fp_ofs WHERE COALESCE(Date_receipt ,0) != 0 GROUP BY Cust_name ) as FP_OFS 
			ON CustIniBal.Cust_ID =  FP_OFS.Cust_name
			LEFT JOIN 
			(SELECT Price, Cust_name , COALESCE(sum(Price),0) as IFS_SUM  FROM fp_ifs WHERE COALESCE(Date_receipt ,0) != 0  GROUP BY Cust_name) as FP_IFS
		    ON CustIniBal.Cust_ID = FP_IFS.Cust_name
		    LEFT JOIN
		    (SELECT Cust_Cash_ID,Customer_ID, dateRequest, sum(Amount) as TotalCachIN FROM customer_cash_in GROUP BY Customer_ID ) as CUST_CASH_IN 
		    ON CustIniBal.Cust_ID = CUST_CASH_IN.Customer_ID 
		    ORDER BY CustIniBal.Cust_ID
		    ");

		$FindCustTran->execute();
		
		return $FindCustTran; 
	}
}	



 ?>