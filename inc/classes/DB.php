<?php
	if(!defined('_CONFIG_')){
			exit("there is no config file");
		}//this for security inorder that no one can run that without the index
 /**
  * 
  */
 class DB
 {
 	protected static $con;  //to access the static variable we use self::
 	// protected as it is in that class only
 	private function __construct()
 	{
 		try {
 			self::$con = new PDO('mysql:host=localhost;charset=utf8mb4;port=3306;dbname=login_course','TCWD','Password1234');
 			self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 			self::$con->setAttribute(PDO::ATTR_PERSISTENT, false);
 		} catch (PDOException $e) {
 			echo "Could not connect to database. \r\n";
 			exit;
 		}//to connect to database throw PDO command

 	}//end of private function
 	public static function getConnection() {
 		//if this instance was not been started, start it
 		if (!self::$con){
 			new DB();
 		}
 		//return the writeable db connection
 		return self::$con;
 	} // this public function is called from config to connect to DB
 } //end of class (OOP)
 ?>