<?php
	if(!defined('_CONFIG_')){
			exit("there is no config file");
		}//this for security inorder that no one can run that without the index
 /**
  * 
  */
 class page 
 {

 	static function forceLogin() { //if there is no session set yet then go to login to set up a session
		if (!isset ($_SESSION['user_id'])) {
		header("Location:/login.php"); exit;  //this how we redirect in PHP
	 
		}	
	}
	static function forceDashboard() { //if there is a session set then go to dashboad, no login is allowed!
		if (isset ($_SESSION['user_id'])){
		header("Location: /dashboard"); exit;  // this how we redirect in PHP
		}
	}
 }

 ?>