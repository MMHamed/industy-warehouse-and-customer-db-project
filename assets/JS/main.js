
//--------------- Register js  -----------------
$(document) //DOM (document object model)
.on("submit" , "form.js-Register", function(event){  //jquery.on() method
	// note that form class-name can't contain any "."
	event.preventDefault();
	var _form = $(this); // put the form (this) in a variable
	var _error = $(".js-error", _form)
	var dataObj = {  //create an object in oop that contain all input 
		email : $("input[type='email']", _form).val(),
		password :  $("input[type='password']", _form).val()
	};

   if (dataObj.email.length < 6){
   		_error
   		.text( "the email length shouldn't be less then 6")
   		.show();
   		return false;
   } else if (dataObj.password.length < 11){
   		_error
   		.text( "the password length shouldn't be less then 11")
   		.show();
   		return false;

   }
	//console.log(dataObj); //to track the varialbe on the consol (f12)
    _error.hide();  //if we pass both if there is no need that any error position appear

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/register.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.redirect !== undefined){   //data return from AJAX/register (return array)[redirect]
      window.location = data.redirect;  //this is how we redirect in JS
      
   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      _error
      .text(data.error)
      .show();
   }
   
})	
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
})

// -----------------login js--------------------------

$(document) //DOM (document object model)
.on("submit" , "form.js-Login", function(event){  //jquery.on() method
   // note that form class-name can't contain any "."
   event.preventDefault();
   var _form = $(this); // put the form (this) in a variable
   var _error = $(".js-error", _form)
   var dataObj = {  //create an object in oop that contain all input 
      email : $("input[type='email']", _form).val(),
      password :  $("input[type='password']", _form).val()
   };

   if (dataObj.email.length < 6){
         _error
         .text( "the email length shouldn't be less then 6")
         .show();
         return false;
   } else if (dataObj.password.length < 11){
         _error
         .text( "the password length shouldn't be less then 11")
         .show();
         return false;

   }
   //console.log(dataObj); //to track the varialbe on the consol (f12)
    _error.hide();  //if we pass both if there is no need that any error position appear

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/login.php',  //the file in the server that manipulate the dataObj
   // we can write it as if statment for both login and register
   // (_form.hasClass('JS.login')? '/ajax/login.php' : '/ajax/register.php' )
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if(data.redirect !== undefined){   //data return from AJAX/register (return array)[redirect]
      window.location = data.redirect;  //this is how we redirect in JS
      
   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      _error
      .html(data.error) // as we return <a> for the link to make a new register.
      .show();
   }
   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) // -----------------end of login js------------------- 


// ---------------Customer Cash IN form ----------------
$(document) //DOM (document object model)
.on("submit" , "form.CashIN", function(event){  //jquery.on() method
   // note that form class-name can't contain any "."
   //event.preventDefault();

   var _form = $(this); // put the form (this) in a variable
   var dataObj = {  //create an object in oop that contain all input 
      Date : $("#Date").val(),
      CustID :  $("select", _form).val(),
      Amount:   $("input[type='number']",_form).val(),
      Comment:  $("textarea",_form).val()
   };
   
   if (dataObj.CustID == ""){
      $('#CustDetail').html('Please select a customer').show();
      $('#CustDetail').css({"color":"red"});
      return false;
   };
   

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/CashIN.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.message !== undefined){   //data return from AJAX/register (return array)[redirect]
      $("button[type='submit']",_form).html(data.message) ;
      $("button[type='submit']",_form).css({"background":"yellow", "color":"black"});
      $("#NR").show();
      $("#DB").show();
   }   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //-------- end of Customer Cash IN form ------------------

// ---------------Add New form (customer,FP,RM) ----------------
$(document) //DOM (document object model)
.on("submit" , "form.AddNew", function(event){  //jquery.on() method
   // note that form class-name can't contain any "."
   //event.preventDefault();

   var _form = $(this); // put the form (this) in a variable
   var _formID = $(this).attr('id');
   
   switch(_formID){
      case "Customer":
         var dataObj = {   
            Date : $("#Date").val(),
            formID: _formID,
            CustName: $("input[id='CustName']",_form).val(),
            CustAdderss: $("input[id='CustAdderss']",_form).val(),
            CustTel: $("input[id='CustTel']",_form).val(),
            CustContact:$("input[id='CustContact']",_form).val(),
            InitialBalance: $("input[type='number']",_form).val()
          };
       break;
       case "FinishProduct":
          var dataObj = {   
            Date : $("#Date").val(),
            formID: _formID,
            FPName:$("input[type='text']",_form).val(),
            InitialBalance: $("input[type='number']",_form).val()
          };
       break;
       case "RawMaterial":
         if($("select",_form).val() == ""){
          alert('please select Group');
          return false;
         }
          var dataObj = {   
            Date : $("#Date").val(),
            formID: _formID,
            RMGroup:$("select",_form).val(),
            RMName: $("#RMat").val(),
            InitialBalance: $("input[type='number']",_form).val()
          };
       break; 
   };

  console.log(dataObj); 

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/AddNew.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.message !== undefined){   //data return from AJAX/register (return array)[redirect]
      $("button[type='submit']",_form).html(data.message) ;
      $("button[type='submit']",_form).css({"background":"yellow", "color":"black"});
      $("#NR").show();
      $("#DB").show();
   }   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //-------- end of Add New form (customer,FP,RM) ------------------


/*********************************************************
*********Finish Product listeners ***********************
*********************************************************/


// ------OutForSaleForm/JS-customer list && customer balance by customer name--------

   $('.OFScust').change(function(){  //jquery.on() method is for form only. $('select')=$('#cell12')=$('.cell12')==> we get the same result for tag/class/ID
   var TableID = $('table').attr('ID');
   var dataObj = {  //create an object in oop that contain all input 
      custName : $(this).val()
      };
   
   
   $.ajax({  //we will send bulbT(the selected bulbTypr) to the PHP file on the server  
      type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
      url:'/ajax/OFScustomer.php',  //the file in the server that manipulate the dataObj
      // we can write it as if statment for both login and register
      // (_form.hasClass('JS.login')? '/ajax/login.php' : '/ajax/register.php' )
      data: dataObj,  //email $ password
      DataType: 'json',  
      async: true  //data transfer type
   })
   .done(function ajaxDone(data){  //action if no error
   console.log(data);
      if(data.detail !== undefined){   //data return from AJAX/register (return array)[redirect]
         $('#CustDetail')
         .html(data.detail)
         .show();
         $('#CustDetail').css({"color":"black"});
          
          if (TableID == 'CustTableByCust'){
               
               $.ajax({  
                type:'POST',  
                url:'/ajax/CustomerTran.php',  
                data: dataObj, 
                DataType: 'json',  
                async: true  //data transfer type
              })
              .done(function ajaxDone(data1){  //action if no error
               console.log(data1);
                 if(data1 !== undefined){
                    
                        $("table#CustTableByCust tbody").find('tr').each(function(){
                           $(this).remove();
                        }) //to remove all the row 'tr ' in the body of the table
                            
                           
                        var i = 1;  
                        $.each(data1, function(key , value) {//to get code, group, raw material in the table
                
                                  if(key != "Balance"){                         
                                    var markup = "<tr><td style= 'border:1px solid black'>"+ i +"</td><td style= 'border:1px solid black'>" + value[0] + "</td><td style= 'border:1px solid black'>"+value[1]+"</td><td style= 'border:1px solid black'>" + value[2] + "</td><td style= 'border:1px solid black'>" + value[3] + "</td> <td style= 'border:1px solid black'>" + value[4] + "</td> <td style= 'border:1px solid black'>" + value[5] + "</td></tr>";
                                    $("table#CustTableByCust tbody").append(markup);
                                
                                 i++;
                                 }
                              })
                      $('#CusTotal').html(data1.Balance);
                      if (data1.Balance < 0){
                       $('#CusTotal').css({"color":"red", "font-weight":"blod","font-size":"30px"}); 
                      }

                      }else{

                          $("table#CustTableByCust tbody").find('tr').each(function(){
                           $(this).remove();
                         //to remove all the row 'tr ' in the body of the table
                      })
                      }

                     if (data1.error !== undefined) { //data return from AJAX/register array(return[error])
                        alert ('data1.error');
                     }
                     })
                     

               .fail(function ajaxFail(e){  //action if ajax fail
                     console.log(e);
                     })
               .always(function ajaxAlwaysDoThis(data){  //action always
                     console.log('Always');
                     })

               
            }
               
      }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
         alert ('data.error');
      }
          
      
   }) 
   .fail(function ajaxFail(e){  //action if ajax fail
   console.log(e);
   })
   .always(function ajaxAlwaysDoThis(data){  //action always
   console.log('Always');
   })
      return false;
 })  //------end of--OutForSaleForm/JS-customer list && customer balance by customer name --------




// ------OutForSaleForm/SELECT-row I change------------

$('.SELECTI').change(function(){  //jquery.on() method is for form only. $('select')=$('#cell12')=$('.cell12')==> we get the same result for tag/class/ID
   var cell = $(this).parent("td"); //to get the cell of the selector (td)
   var row =  $(cell).parent("tr"); //to get the row of the selector (tr)
   var TableID = $(row).parent().parent("table").attr('ID');
   var cellIn = cell[0].cellIndex;
   
   var CodeCellID =  document.getElementById(TableID).rows[row[0].rowIndex].cells[(cell[0].cellIndex) -1].id;//.children("select");
            // to get the ID of the  cell 1 
   var BalanceCellID = document.getElementById(TableID).rows[row[0].rowIndex].cells[(cell[0].cellIndex) +2].id;//.children("select");
            // to get the ID of the  cell 4 
   

   var _Code = $("#"+ CodeCellID);
   var _BAL = $("#"+ BalanceCellID);
   var Sarr = [$("#Scell12"), $("#Scell22"),$("#Scell32"),$("#Scell42"),$("#Scell52"),$("#Scell62"),$("#Scell72"),$("#Scell82"),$("#Scell92"),$("#Scell102")];
   var dataObj = {  //create an object in oop that contain all input 
      bulbT : $(this).val()
      };
               

     for (i=0; i<9; i++){
   if (dataObj.bulbT==Sarr[i].val() && Sarr[i].val()!=undefined ){
      
      Sarr[i].parent().css({"background":"red"});
      $(this).parent().css({"background":"red"});
      
   }else{
      $(this).parent().css({"background":"#f0f0f0"});
      Sarr[i].parent().css({"background":"#f0f0f0"});
   }
   }

     
$.ajax({  //we will send bulbT(the selected bulbTypr) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/OFS_FPselect.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if(data.cell1 !== undefined){   //data return from AJAX/register (return array)[redirect]
      _Code
      .html(data.cell1);
      _BAL
      .html(data.cell4); //to change the contain of cell 12 & cell 15 in OutForSaleForm
      
   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      alert ('data.error');
   }
   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //---------end of select row I change-----------


//--------------input Icell 3 change row I---------

$('.INPUTI').change(function(){
   
       
         
        var cell = $(this).parent().parent().parent("td"); //to get the cell of the selector (td)
        var row =  $(cell).parent("tr"); //to get the row of the selector (tr)
        var TableID = $(row).parent().parent("table").attr('ID');
        

        var Icell3ID = $(this);
        var cell2ID = document.getElementById(TableID).rows[row[0].rowIndex].cells[(cell[0].cellIndex) - 1].id;
        var Scell2ID= $("#" + cell2ID).children().attr('id');
        var cell4ID = document.getElementById(TableID).rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 1].id;               
               
         var dataObj = {  //create an object in oop that contain all input 
               cell4 : $('#'+cell4ID).html(), 
               cell3 : $(this).val(),
               cell2 : $('#'+Scell2ID).val(),
               TableID : TableID,
            };
   
   
   $.ajax({  //we will send bulbT(the selected bulbTypr) to the PHP file on the server  
      type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
      url:'/ajax/OFSbalance.php',  //the file in the server that manipulate the dataObj
      // we can write it as if statment for both login and register
      // (_form.hasClass('JS.login')? '/ajax/login.php' : '/ajax/register.php' )
      data: dataObj,  //email $ password
      DataType: 'json',  
      async: true  //data transfer type
   })
   .done(function ajaxDone(data){  //action if no error
   console.log(data);
      if(data.cell4 !== undefined ){   //data return from AJAX/register (return array)[redirect]
           
      $('#'+cell4ID).html(data.cell4);  //to change the contain of cell 14 in OFS_FPselect

           if (data.cell4 < 0 ){
            Icell3ID.css({"background":"red"});
           }else{
            Icell3ID.css({"background":"white"});
           }

      }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
         alert ('data.error');
      }
      
   }) 
   .fail(function ajaxFail(e){  //action if ajax fail
   console.log(e);
   })
   .always(function ajaxAlwaysDoThis(data){  //action always
   console.log('Always');
   })
      return false;
}) //--------end of input Icell 3 change row I------------- 



//--------OFS Submit -----------
 $('.FPsubmit').click(function(){
   event.preventDefault();
   
     _SubID = $(this).attr('ID');

     var _res = new Array();
     var i = 0;
         var y = i+1;
         
         while ($('#Scell'+y+'2').val() != '' && $('#Icell'+y+'3').val() != ''){
            
                _res[i] = [$('#Scell'+y+'2').val() , $('#Icell'+y+'3').val() ];//multi_dimensional array
               
               i++
               y = i+1;
               };
         if (i == 0){
           alert('please select at least one Item and quantity'); 
           return false; 
         }
         
          
         if ($('#OFScust').val() == ''){
            alert ('please select a customer');
            return false;
         };
   
   var dataObj = {  //create an object in oop that contain all input 
      Date :$('#Date').val(),
      custName : $('#OFScust').val(),
      tran: _res,
      index:i,
      SubID:_SubID 
   };

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/FPsubmit.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.added !== undefined){   //data return from AJAX/register (return array)[redirect]
     $('#'+_SubID)
      .html(data.added);   //this is how we redirect in JS
     $('#'+_SubID)
      .css({"background":"yellow"});
      $('#OFS_FNum').html(data.formNum);
      $('#NR').show();

   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      alert ('data.error');
   }
   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //---end of OFS submit ----- 

//-------------new request buttom---------
$('#NR').click(function(){
   location.reload(true);
})//-----end  new request buttom-------- 

//-------------FP approve Got to DashBoard ---------
$('#DB').click(function(){
   window.location.assign("/Dashboard.php");
})//-----end FP approve Got to DashBoard-------- 

//--------FP Approve/reject Buttons  -----------
 $('.FPApRj').click(function(){
   event.preventDefault();
   var ButtonID = $(this).attr('ID');
   
   
            var _res = new Array();

            var _ReqNum = document.getElementById("FPapp_FNum").innerHTML;
            var RowNum = $('#FPATable tbody tr').length;
            
             var i = 0;
             var y = i+1; 
             while($('#IAcell'+y+'3').val() != 0 &&  y <= RowNum ){
                  
                 _res[i] = [$('#SAcell'+y+'2').val() , $('#IAcell'+y+'3').val()];//multi_dimensional array
                 i++
                 y++       
               };
               var RowNum= i;
              
            var dataObj = {  //create an object in oop that contain all input 
               Date :$('#ADate').val(),
               custName : $('#FPAcust').val(),
               TableName : $('#TableName').html(),
               ButtonID : ButtonID,
               tran: _res,
               index : RowNum,
               ReqNum:_ReqNum 
            };
             
                        
         $.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/FPAppReq.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if (data.approve !== undefined){   //data return from AJAX/register (return array)[redirect]
              $('#'+ ButtonID)
               .html(data.approve);   //this is how to change the innerHTML
              $('#' + ButtonID)
               .css({"background":"yellow"});
               $('#NA').show();
               

            }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })
            return false;

      
})
//---end of FP Approve/reject Buttons ----- 

//-------------FP go to new appove list button---------
$('#NA').click(function(){
   window.location.assign("/DashboardForm/FPNeedForAppovalList.php");
})//-----end FP new approve list buttom-------- 

//----------FP Unit price Receipt From ----------------
$(".UPrice").change(function(){ 
  var UPriceID = $(this).attr('id');
  var cell = $(this).parent().parent().parent("td"); //to get the cell element of the selector (td)
  var row =  $(cell).parent("tr"); //to get the row element of the selector (tr) 

  var cell5ID = document.getElementById('FPRTable').rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 1].id;
  var cell3ID = document.getElementById('FPRTable').rows[row[0].rowIndex].cells[(cell[0].cellIndex) - 1].id;
   $('#'+cell5ID).html( $('#'+cell3ID).html() * $(this).val());
   
   var value = 0;
   $(".SubTotal").each(function(){
      value = value + Number($(this).html());
      });
   $('#RTotal').html(value + " EGP");
   
})//---------- End FP Unit price Receipt From ----------------

// ------------------Print/submit receipt button --------------
$('#FPReceipt').click(function(){
   event.preventDefault();
            var _res = new Array();

            var _ReqNum = document.getElementById("FPapp_FNum").innerHTML;
            var RowNum = $('#FPRTable tbody tr').length;
            
             var i = 0;
             var y = i+1; 
             while(y <= RowNum ){
                  
                 _res[i] = $('#Acell'+y+'5').html();//multi_dimensional array
                 i++
                 y++       
               };
               var RowNum= i;
              
            var dataObj = {  //create an object in oop that contain all input 
               Date :$('#ADate').val(),
               custName : $('#FPRcust').val(),
               TableName : $('#TableName').html(),
               tran: _res,
               index : RowNum,
               ReqNum:_ReqNum 
            };
             
                        
         $.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/FPReceipt.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if (data.approve !== undefined){   //data return from AJAX/register (return array)[redirect]
              alert(data.approve);   //this is how to change the innerHTML
              window.print();
              if(!confirm('Finished printing?')) return false;  //this to make redirection after .print event (OK) and not cancel !
              // window.onafterprint = function(e){  //this didn't work !!
               window.location.replace ("/DashboardForm/FPNeedForCustReceiptList.php");
              //};
               //var divToPrint=document.getElementById("TBprint");
               //newWin = window.open("/DashboardForm/PrintForm.php","","","");
               //newWin.body.append(divToPrint);
               //newWin.document.write(divToPrint.outerHTML);
               //newWin.print();
               //newWin.close();
               
               

            }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })
            return false;
 })
      // ------- end Print/submit receipt button -------------- 


/******************************************************************
 ------------RmProdReqForm listener -----------------------------
********************************************************************/

// ------------Row Material on .select1 and .select2 -----------------
$('table#RMRtbl select').change(function(){
   //get row and collumn for the element (select) that change
   var cell = $(this).parent("td"); //to get the cell of the selector (td)
   var row =  $(cell).parent("tr"); //to get the row of the selector (tr)
   
   var cellIn = cell[0].cellIndex;
   
   switch (cellIn){
      case(2): //it is .select1 (cell #2) change .select2

            var NextCellID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) +1].id;//.children("select");
            // to get the ID of the next cell (that contain the new select) given the cell and row index
            select2ID = $("#" + NextCellID).children().attr('id'); //to get tne id of the new select give the id of its parent as variable
             
            var dataObj = {  //create an object in oop that contain all input 
                  GrpName : $(this).val()
                 };
               
             
            $.ajax({  //we will send bulbT(the selected bulbTypr) to the PHP file on the server  
               type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
               url:'/ajax/RM_GrpSelect.php',  //the file in the server that manipulate the dataObj
               // we can write it as if statment for both login and register
               // (_form.hasClass('JS.login')? '/ajax/login.php' : '/ajax/register.php' )
               data: dataObj,  //email $ password
               DataType: 'json',  
               async: true  //data transfer type
            })
            .done(function ajaxDone(data){  //action if no error
            console.log(data);
               if(data !== undefined){
                  
                  $("#" + select2ID).empty();
                   //add an empty option in order make the selection default value empty.
                  var newOptions =$('<option>').attr('value', "").text("");
                  $("#" + select2ID).append(newOptions).trigger("chosen:updated");
                 
                 $.each(data, function(key , value) {

                      newOptions =$('<option>').attr('value', key).text(value);
                     
                     $("#" + select2ID).append(newOptions).trigger("chosen:updated");
                      
                 });

             }else{

                 $("#" + select2ID).empty();

             }

                if (data.error !== undefined) { //data return from AJAX/register array(return[error])
                  alert (data.error);
               }
               
            }) 
            .fail(function ajaxFail(e){  //action if ajax fail
            console.log(e);
            })
            .always(function ajaxAlwaysDoThis(data){  //action always
            console.log('Always');
            })
               return false;

         break;

         case(3)://it is .select2 (cell # 3) change cell 1 & 6

            var CodeCellID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) -2].id;//.children("select");
            var BalanceCellID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 3].id;

            var _Code = $("#"+ CodeCellID);
            var _BAL = $("#"+ BalanceCellID);
            var Sarr = [$("#RMScell13"), $("#RMScell23"),$("#RMScell33"),$("#RMScell43"),$("#RMScell53"),$("#RMScell63"),$("#RMScell73"),$("#RMScell83"),$("#RMScell93"),$("#RMScell103")];
            var dataObj = {  //create an object in oop that contain all input 
               RawM : $(this).val()
               };
               

              for (i=0; i<9; i++){
            if (dataObj.RawM==Sarr[i].val() && Sarr[i].val()!=undefined ){
               
               Sarr[i].parent().css({"background":"red"});
               $(this).parent().css({"background":"red"});
               
            }else{
               $(this).parent().css({"background":"#f0f0f0"});
               Sarr[i].parent().css({"background":"#f0f0f0"});
            }
            }
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/Pro_RMselect.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data.cell1 !== undefined){   //data return from AJAX/register (return array)[redirect]
               _Code
               .html(data.cell1);
               _BAL
               .html(data.cell6); //to change the contain of cell 12 & cell 15 in OutForSaleForm
               
            }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })
            return false;

      break;

   };

}) //--------end of Row Material on .select1 and .select2 -------------



//--------------input .RMIcell4 .RMIcell5 change cell6 (.Balance) ---------

$('table#RMRtbl input').change(function(){
   //get row and collumn for the element (select) that change
   var cell = $(this).parent().parent().parent("td"); //to get the cell of the selector (td)
   var row =  $(cell).parent("tr"); //to get the row of the selector (tr)
   
   var cellIn = cell[0].cellIndex;
   

   switch (cellIn){
      case(4): //it is RMIcell4 (cell #4) change cell6 (.Balance)
              var cell3ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) - 1].id;
              var Scell3ID= $("#" + cell3ID).children().attr('id');
              var cell5ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 1].id;
              var Icell5ID= $("#" + cell5ID).children().children().children("input").attr('id');
              var cell6ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 2].id;               
              
               var cell4 = $(this).val();
               var cell5 = $("#" + Icell5ID).val();
               var cell6 = $("#" + cell6ID).val();
               
               var dataObj = {  //create an object in oop that contain all input 
               RawM : $("#" + Scell3ID).val()
               };
             
              
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/Pro_RMselect.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data.cell6 !== undefined){   //data return from AJAX/register (return array)[redirect]
               if (cell5 == undefined){cell5=0};
               if (cell4 == undefined){cell4=0};
               cell6 = Number(data.cell6) - Number(cell4) + Number(cell5);
               $("#"+cell6ID).html(cell6);
         
                 if (data.cell6 < 0 ){
                  $(this).css({"background":"red"});
                 }else{
                  $(this).css({"background":"white"});
                 }
            }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })
               

         break;

       case(5):
              var cell3ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) - 2].id;
              var Scell3ID= $("#" + cell3ID).children().attr('id');
              var cell4ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) - 1].id;
              var Icell4ID= $("#" + cell4ID).children().children().children("input").attr('id');
              var cell6ID = document.getElementById('RMRtbl').rows[row[0].rowIndex].cells[(cell[0].cellIndex) + 1].id;               
              
               var cell4 = $("#" + Icell4ID).val();
               var cell5 = $(this).val();
               var cell6 = $("#" + cell6ID).val();
               
               var dataObj = {  //create an object in oop that contain all input 
               RawM : $("#" + Scell3ID).val()
               };
             
              
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/Pro_RMselect.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data.cell6 !== undefined){   //data return from AJAX/register (return array)[redirect]
               if (cell5 == undefined){cell5=0};
               if (cell4 == undefined){cell4=0};
               cell6= Number(data.cell6) - Number(cell4) + Number(cell5);
               
               $("#"+cell6ID).html(cell6);
         
                 if (data.cell6 < 0 ){
                  $(this).css({"background":"red"});
                 }else{
                  $(this).css({"background":"white"});
                 }
            }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })
               
               
         break;
         }     
            return false;
}) //--------end of input .RMIcell4 .RMIcell5 change cell6 (.Balance)------------- 


// ----------------RMByBulbType  Bulb Type change ----------------
$('#SelBT').change(function(){
      
               var dataObj = {  //create an object in oop that contain all input 
               SelBT : $(this).val()
               };
             
              
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/RMByBTSelect.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  //email $ password
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data !== undefined){
                  
                  $('#SelBSC').empty();
                   //add an empty option in order make the selection default value empty.
                  var newOptions =$('<option>').attr('value', "").text("");
                  $('#SelBSC').append(newOptions).trigger("chosen:updated");
                 
                 $.each(data, function(key , value) {

                      newOptions =$('<option>').attr('value', value).text(value);
                     
                     $('#SelBSC').append(newOptions).trigger("chosen:updated");
                      
                 });

             }else{

                 $("#" + select2ID).empty();

             }
            if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })

      })//-----end of RMByBulbType  Bulb Type change ---------------
      
// ----------------RMByBulbType  Body SKD CKD change ----------------
$('#SelBSC').change(function(){
            
               var dataObj = {  //create an object in oop that contain all input 
               SelBSC : $(this).val(),
               SelBT: $(SelBT).val()
               };
            
              
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/RMByBSCSelect.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data !== undefined){
                  
               $("table#RMBSCtbl tbody").find('tr').each(function(){
                  $(this).remove();
               }) //to remove all the row 'tr ' in the body of the table
                   
                  
               var i = 1;  
               $.each(data, function(key , value) {//to get code, group, raw material in the table
       
                  //to get the balance
                  var dataObj1 = {  //create an object in oop that contain all input 
                   RawM : key
                    };
                   $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
                        type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
                        url:'/ajax/Pro_RMselect.php',  //the file in the server that manipulate the dataObj
                        data: dataObj1,  
                        DataType: 'json',  
                        async: true  //data transfer type
                     })
                     .done(function ajaxDone(data1){  //action if no error
                     console.log(data1);

                        var markup = "<tr><td style= 'border:1px solid black'>"+i+"</td><td style= 'border:1px solid black' id='cod"+i+"'>" + key + "</td><td style= 'border:1px solid black'>"+value[0]+"</td><td style= 'border:1px solid black'>" + value[1] + "</td><td class = 'inpByBT' id='QByBT"+i+"' style= 'border:1px solid black'>0</td> <td style= 'border:1px solid black' id='Bal"+i+"'>"+ data1.cell6 +"</td></tr>";
                        $("table#RMBSCtbl tbody").append(markup);
                        i++;
                     })
                     .fail(function ajaxFail(e){  //action if ajax fail
                     console.log(e);
                        })
                })
             }else{

                 $("table#RMBSCtbl tbody").find('tr').each(function(){
                  $(this).remove();
               }) //to remove all the row 'tr ' in the body of the table

             }
            if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })

      })//-----end of RMByBulbType  Body SKD CKD change ---------------   

//--------------RMByBulbType quantity change ------------------
 $('#ISelBSC').change(function(){
   var i=1;
   $.each($('.inpByBT'),function() {
    $(this).html($('#ISelBSC').val());
    var OutQu = $(this);
    var bal = $('#Bal'+i);
    var dataObj = {  //create an object 
                   RawM : $('#cod'+i).html()
                    };

                    $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
                        type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
                        url:'/ajax/Pro_RMselect.php',  //the file in the server that manipulate the dataObj
                        data: dataObj,  //email $ password
                        DataType: 'json',  
                        async: true  //data transfer type
                     })
                     .done(function ajaxDone(data){  //action if no error
                     console.log(data);
                     bal.html(data.cell6 - OutQu.html());
                     if (bal.html() < 0){
                         OutQu.css({"background":"red"});
                     }else{
                        OutQu.css({"background":"white"});
                        }
                     })
       i++; 
     })
 })//--------------end of RMByBulbType quantity change ------------------
 
 //--------RM Submit -----------
 $('.RMsubmit').click(function(){
   event.preventDefault();
   var RMS_ID= $(this).attr('id');
   var _res = new Array(); //define an array
   var RowNum = 0;
   if (RMS_ID == 'RMByBTsubmit'){
         RowNum= $('#RMBSCtbl tbody tr').length;//to get the bumber of row in the body of table
             
         for (i=0; i< RowNum; i++){
            y=i+1
                _res[i] = [$('#cod'+y).html() , $('#QByBT'+y).html(),0];//multi_dimensiona array
               
                  };
           

          if(RowNum==0 || $('#ISelBSC').val()==0) { 
            alert('please select at least one Item and quantity'); 
           return false;
            };
   } else if(RMS_ID == 'RMsubmit'){
         var i = 0;
         var y = i+1;
         
         while ($('#RMScell'+y+'3').val() != '' && ($('#Icell'+y+'4').val() != ''   || $('#Icell'+y+'5').val() != '')){
            
                _res[i] = [$('#RMScell'+y+'3').val() , $('#Icell'+y+'4').val() , $('#Icell'+y+'5').val()];//multi_dimensiona array
               
               i++
               y = i+1;
               };
         if (i == 0){
           alert('please select at least one Item and quantity'); 
           return false; 
         }
         RowNum = i;
      } 


   var dataObj = {  //create an object in oop that contain all input 
      Date :$('#RMDate').val(),
      tran: _res,
      index: RowNum 
   };
console.log(dataObj);
$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/RMrequest.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.added !== undefined){   //data return from AJAX/register (return array)[redirect]
     $('#'+ RMS_ID)
      .html(data.added);   //this is how we redirect in JS
     $('#'+ RMS_ID)
      .css({"background":"yellow"});
      $('#RM_FNum').html(data.formNum);
      $('#NR').show();

   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      alert ('data.error');
   }
   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //---end of RM submit ----- */

//--------------------RM Approve ---------------------
 $('#RMApprove').click(function(){
   event.preventDefault();
   
   var _res = new Array();
   var _ReqNum = document.getElementById("RMapp_FNum").innerHTML;
   var RowNum= $('#RMATable tbody tr').length;
    for (i=0; i< RowNum; i++){
         y=i+1;
           
         if ($('#IAcell'+y+'3').val() == 0 && $('#IAcell'+y+'4').val() == 0){
        _res[i] = [$('#IAcell'+y+'3').val() , $('#IAcell'+y+'4').val()];
         break;
      }else{
        _res[i] = [$('#IAcell'+y+'3').val() , $('#IAcell'+y+'4').val()];//multi_dimensiona array
       }        
      };

      
   var dataObj = {  //create an object in oop that contain all input 
      Date :$('#ADate').val(),
      tran: _res,
      index:i,
      ReqNum:_ReqNum 
   };
    
    

$.ajax({  //we will send the object (contain the 2 variable) to the PHP file on the server  
   type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
   url:'/ajax/RMAppReq.php',  //the file in the server that manipulate the dataObj
   data: dataObj,  //email $ password
   DataType: 'json',  
   async: true  //data transfer type
})
.done(function ajaxDone(data){  //action if no error
console.log(data);
   if (data.approve !== undefined){   //data return from AJAX/register (return array)[redirect]
     $('#RMApprove')
      .html(data.approve);   //this is how to change the innerHTML
     $('#RMApprove')
      .css({"background":"yellow"});
      $('#RMNA').show();
      

   }else if (data.error !== undefined) { //data return from AJAX/register array(return[error])
      alert ('data.error');
   }
   
}) 
.fail(function ajaxFail(e){  //action if ajax fail
console.log(e);
})
.always(function ajaxAlwaysDoThis(data){  //action always
console.log('Always');
})
   return false;
}) //---end of Raw Material approved ----- 

//-------------OFS go to new appove list button---------
$('#RMNA').click(function(){
   window.location.assign("/DashboardForm/RMNeedForAppovalList.php");
})//-----end OFS new approve list buttom-------- 


// ----------------RM Balance Report By Group ----------------
$('#SelByGr').change(function(){
            
               var dataObj = {  //create an object in oop that contain all input 
               SelGroup : $(this).val(),
                 };
            
              
         $.ajax({  //we will send RawM(the selected bulbTypr) to the PHP file on the server  
            type:'POST',  //post is beter then get as it is more secure. it send the data and remove it from html
            url:'/ajax/RMReportByGroup.php',  //the file in the server that manipulate the dataObj
            data: dataObj,  
            DataType: 'json',  
            async: true  //data transfer type
         })
         .done(function ajaxDone(data){  //action if no error
         console.log(data);
            if(data !== undefined){
                  
               $("table#RMBalRepByG tbody").find('tr').each(function(){
                  $(this).remove();
               }) //to remove all the row 'tr ' in the body of the table
                   
                  
               var i = 1;  
               $.each(data, function(key , value) {//to get code, group, raw material in the table
       
                  

                        var markup = "<tr><td style= 'border:1px solid black'>"+i+"</td><td style= 'border:1px solid black' id='cod"+i+"'>" + key + "</td><td style= 'border:1px solid black'>"+value[0]+"</td><td style= 'border:1px solid black'>" + value[1] + "</td><td style= 'border:1px solid black'>" + value[2] + "</td></tr>";
                        $("table#RMBalRepByG tbody").append(markup);
                        i++;
                     
                     
                })
             }else{

                 $("table#RMBalRepByG tbody").find('tr').each(function(){
                  $(this).remove();
               }) //to remove all the row 'tr ' in the body of the table

             }
            if (data.error !== undefined) { //data return from AJAX/register array(return[error])
               alert ('data.error');
            }
            
         }) 
         .fail(function ajaxFail(e){  //action if ajax fail
         console.log(e);
         })
         .always(function ajaxAlwaysDoThis(data){  //action always
         console.log('Always');
         })

      })//------- end of RM Balance Report By Group ------------------    
 