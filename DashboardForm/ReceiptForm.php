<head>
<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
  <div ID = 'TBprint'>
      <title>ReceiptForm</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      <style type="text/css" media="print">
        .no-print { display: none; }
        @media print {
            pre, blockquote {page-break-inside: avoid;}
        }
     </style>
     <style>
      .header {max-height:60px;}
      tr {max-height:10px;}
     </style> 

 </head>
 <body>
   
      <div class="uk-container header ">
          <div class= 'uk-grid '>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3 "> <h1 class="uk-card-title ">  Receipt</h1> <br /> 
                    <?php 
                      switch($_POST['FPTableName']){
                        case "fp_ofs":
                             print  "Out For Sales";
                        break;
                        case "fp_ifs":
                             print  "In From Sale";
                        break;
                        
                      }
                      ?> 
                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 ">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            

    <div class="uk-container header">
          <div class= "uk-grid ">
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1 " ID="FPapp_FNum" >
                      <?php 
                        print $_POST['FPToBeApporve'];
                      ?> 
                  </div> <!--We will receive the form number to be approve here  -->
              
          </div>
    </div> <!--end of container Form Number --> 
     
<div class="uk-container header">
          <div class= 'uk-grid '>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3  ">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3  ">
                  <fieldset class="uk-fieldset  ">

                      <div class="uk-margin ">
                          <input class="uk-input " type="date" ID="ADate" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 ">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->

    <div class="uk-container header">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Delivery to</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3 " id='FPRcust'> 
                        
                                <?php // to get customer name and address

                                      $FormNum = $_POST['FPToBeApporve'];
                                      $TableName = $_POST['FPTableName'];
                                      $result1 = user::FindFormReq($FormNum,$TableName);
                                      $Form_Found =  $result1->fetch(PDO::FETCH_ASSOC);
                                      $CustID = $Form_Found['Cust_name'];
                                      $CustName = user::findCustByID($CustID);
                                       print  $CustName['CustomerName'];                             
                                ?>   
                           

                  </div><!-- end of customer select    -->
                  
             
                  <div class='uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 '>منصرف الى </div>              
          </div>
    </div> <!--end of container delivery to --> 

    <div class='uk-container '>
          <div class= uk-grid>
              
                  <div class='uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1 ' ID='CustDetail'>
                    <?php
                    if ($TableName == 'fp_ifp' ) {
                      print "Address = Industry"; 
                     } else {$Cust_detail=user::findCustByName($Form_Found['Cust_name']);
                    print "Address = ".$Cust_detail['Address']." / "."Phone Number = ".$Cust_detail['PhoneNumber']." / "."Contact Person = ".$Cust_detail['ContactPerson'];
                  };
                   ?>
                  </div> 
    </div>
          </div> <!--  end of customer detail  -->


     <div class="uk-container uk-text-center">
          <div class= uk-grid>
                    <table class="uk-table  uk-table-divider uk-text-center OFSTable " ID= "FPRTable" style= "border:2px solid black">
                  <form class ="uk-form-stacked">
                    <thead>
                        <tr class="row0" style= "border:2px solid black ; height: 70%">
                            <th class="Acell00"style= "border:1px solid black">S/N</th>
                            <th class="Acell01" style= "border:1px solid black">Code</th>
                            <th class="Acell02"style= "border:1px solid black">Item Description</th>
                            <th class="Acell03"style= "border:1px solid black">Quantity</th>
                            <th class="Acell04"style= "border:1px solid black">Unit Price</th>
                            <th class="Acell05"style= "border:1px solid black">Total Price</th>
                            
                        </tr>
                    </thead>
                  </form>
                    <tbody>
                     
                     <!-- row == i ------------>
<?php
     
     $i = 1;
     $result1 = user::FindFormReq($FormNum,$TableName);
    while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
      $ItemDis= user:: findFP($row1[6]);
      print "<tr style= 'border:1px solid black;max-height:20px;'>
      <td style= 'border:1px solid black;height:70%;' ID='Acell".$i."0'>".$i."</td>  
      <td style= 'border:1px solid black;' ID='Acell".$i."1'>".$row1[6]."</td>
      <td style= 'border:1px solid black;' ID='Acell".$i."2'>".$ItemDis['BulbType']."</td>
       <td style= 'border:1px solid black;' ID='Acell".$i."3'>".$row1[7]."</td>
      <td style= 'border:1px solid black;' ID='Acell".$i."4' >
           <div class='uk-margin'>
              <div class='uk-inline' > 
                  <input class='uk-input UPrice' ID ='IRcell".$i."4'  type='text' style='width: 60%' >
              </div> 
          </div>

        </td>
        <td style= 'border:1px solid black;' class = 'SubTotal' ID='Acell".$i."5'>0</td>
                                 
    </tr> ";
     $i++;
    ;}

    ?>
    
    <!-------end of loop row i ------->
                        
                                                                     
                    </tbody>
                </table>
    </div>
           </div> <!--end of table -->


      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-2-3 "><h1 class="uk-card-title">Total Receipt value</h1> </div>             
              
                  
                  

             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 " ID = 'RTotal'> </div>
              
          </div>
    </div> <!--end of container Total -->
    
 </div> <!--End of to be print area -->
    <div class="uk-container">
          <div class= uk-grid >
              
                   <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1">
                    
                    <button class="uk-button uk-button-default no-print" ID="FPReceipt" type="submit" style='margin: 2px;'>Print /Submit </button>
                                     
                    <button class="uk-button uk-button-default no-print" type="button" style='display:none; margin: 2px;' ID="NA" >Go to receipt List?</button>

                    <div class="uk-card uk-card-default uk-card-body uk-text-right no-print" style='display:none;' ID= 'TableName'  ><?php print $TableName ?> </div>


                  </div>

                  </div>
    </div> <!--end of container Print/Submit -->            

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>