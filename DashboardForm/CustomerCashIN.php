<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
      <title>CustomerCashIN</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      

 </head>
 <body>

      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> Customer Cash IN </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            
<div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                  <fieldset class="uk-fieldset">

                      <div class="uk-margin">
                          <input class="uk-input" type="date" ID="Date" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->
    <div class="uk-container">
          <div class= uk-grid>
              
                          
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1"> 
                     <form class= "CashIN">
                      <fieldset class="uk-fieldset">
                     <div class="uk-margin">
                                               
                           <select data-placeholder="Choose one" class="chosen-select OFScust" ID="OFScust" style ="width:80%" > 
                                  <option value=""></option>
                                  
                                  <?php //we return the index $result not the varaible $row as fetch return only 1st row. 
                                      $result = user::CustList();
                                      
                                      while($row = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                                     print "<option value=".$row[0].">". $row[1]. "</option>";
                                     };
                                   ?>
                            </select>                               
                    </div>
                          
                          <label class="uk-form-label" for="form-stacked-text" ID="CustDetail" style="display:none;"></label>

                          <div class="uk-margin">
                              <input class="uk-input uk-form-width-large" type="number" placeholder="Inter the Cash amount here" required="required">
                          </div>

                          <div class="uk-margin">
                              <textarea class="uk-textarea uk-form-width-large" rows="5" placeholder="Comment"></textarea>
                          </div>
                          <div class="uk-margin"> 
                              <button class="uk-botton uk-button-primary uk-button-small" type="submit">SUBMIT</button>   
                                          </div>
                          <div class="uk-margin"> 
                              <button class="uk-botton uk-button-primary uk-button-small" style="display:none;" ID="NR" >New Submit?</button>   
                          </div>
                          <div class="uk-margin"> 
                              <button class="uk-botton uk-button-secondary uk-button-small" style="display:none;" ID="DB"  >DASHBOARD?</button>   
                              </div>  
                      </fieldset>
                  </form>

                  </div><!-- end of customer select    -->
                               
          </div>
    </div> <!--end of container delivery to --> 

             

   
     


      
              
                
             
                  
              
                    

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>