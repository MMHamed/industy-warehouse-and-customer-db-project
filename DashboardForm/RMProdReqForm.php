<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
      <title>RmProdReqForm</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      

 </head>
 <body>

      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> Request Raw Material from Production</div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            

    <div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1" ID="RM_FNum"> Form Number </div>
              
          </div>
    </div> <!--end of container Form Number -->  
<div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                  <fieldset class="uk-fieldset">

                      <div class="uk-margin">
                          <input class="uk-input" type="date" ID="RMDate" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->

    <div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Delivery to</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> Production                   
                  </div><!-- end of customer select    -->
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">منصرف الى </div>              
          </div>
    </div> <!--end of container delivery to --> 

    
     <div class="uk-container uk-text-center">
          <div class= uk-grid>
                    <table class="uk-table  uk-table-divider uk-text-center OFSTable" ID= "RMRtbl" style= "border:2px solid black ">
                  <form class ="uk-form-stacked">
                    <thead>
                        <tr class="row0" style= "border:2px solid black ; height: 70%">
                            <th class="cell00"style= "border:1px solid black">S/N</th>
                            <th class="cell01" style= "border:1px solid black">Code</th>
                            <th class="cell02"style= "border:1px solid black">Group</th>
                            <th class="cell03"style= "border:1px solid black">Item Description </th>
                            <th class="cell04"style= "border:1px solid black">OUT_Quantity</th>
                            <th class="cell05"style= "border:1px solid black">IN_Quantity</th>
                            <th class="cell05"style= "border:1px solid black">Balance</th>
                        </tr>
                    </thead>
                  </form>
                    <tbody>
                     
                     <!-- row == i ------------>
<?php
     
     
    for ($i=1; $i<=10; $i++){
      
      print "<tr>
      <td ID='cell".$i."0'>".$i."</td>  
      <td ID='cell".$i."1' class = 'Code'>Table Data</td>
      <td ID='cell".$i."2'>
       <select data-placeholder='Choose one' class='chosen-select select1' ID= 'RMScell".$i."2'style ='width:180px' >
              <option value=''></option>" 
              
            ?>   
            <?php 
                  $result = user::RMGroup();
                  
                  while($row = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                print "<option value = ".$row[0].">". $row[0]. "</option>";
                 }; 
               
            
       print "</select>   
       </td>
       
        
       <td ID='cell".$i."3'>
       <select data-placeholder='Choose one' class='chosen-select select2' ID='RMScell".$i."3' style ='width:300px' >
              <option ></option> 
              
       </select> 
       </td>      
           
      <td ID='cell".$i."4' >
           <div class='uk-margin'>
              <div class='uk-inline' > 
                  <a class='uk-form-icon' href='#'' uk-icon='icon: pencil'></a>
                  <input class='uk-input inputOut ' ID ='Icell".$i."4'  type='text' style='width:100px'>
            
            </div>
               </div>
        </td>

        <td ID='cell".$i."5' >
           <div class='uk-margin'>
              <div class='uk-inline' > 
                  <a class='uk-form-icon' href='#'' uk-icon='icon: pencil'></a>
                  <input class='uk-input inputIn ' ID ='Icell".$i."5'  type='text' style='width: =100px'>
              </div> 
          </div>

        </td>

        <td ID='cell".$i."6' class= 'Balance'>Table Data</td>                           
    </tr> "
    ;} 
  ?>
       
    <!-------end of loop row i ------->
                        
                                                                     
                    </tbody>
                </table>
    </div>
           </div> <!--end of table -->


      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Stor Manager Signature <br/> <br/> .......................................</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                    
                    <button class="uk-button uk-button-default RMsubmit" ID="RMsubmit" type="submit" style='margin: 1px;'>Submit Request</button>
                   
                    <button class="uk-button uk-button-default " type="button" style='display:none; margin: 1px;' ID="NR" >New request?</button>

                    <button class="uk-button uk-button-default " type="button" style=' margin: 1px;' ID="DB" >DashBoard?</button>
                  
                  </div>
                  
                    
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">Receiver Signature <br/> <br />........................................ </div>
              
          </div>
    </div> <!--end of container header -->            

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>