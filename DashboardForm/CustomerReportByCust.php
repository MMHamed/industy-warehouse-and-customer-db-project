<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
      <title>CustomerReportByCust</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      

 </head>
 <body>

      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> Customer detail Report</div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            
<div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                  <fieldset class="uk-fieldset">

                      <div class="uk-margin">
                          <input class="uk-input" type="date" ID="Date" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->
    <div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Return From </div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> 
                     <div class="uk-margin">
                                               
                           <select data-placeholder="Choose one" class="chosen-select OFScust" ID="OFScust" style ="width:100%" > <!-- you can add multible="true" as attribute to change it to multible choise -->
                                  <option value=""></option>
                                  
                                  <?php //we return the index $result not the varaible $row as fetch return only 1st row. 
                                      $result = user::CustList();
                                      
                                      while($row = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                                     print "<option value=".$row[0].">". $row[1]. "</option>";
                                     };
                                   ?>
                            </select>                               
                    </div>

                  </div><!-- end of customer select    -->
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3"> مرتجع من  </div>              
          </div>
    </div> <!--end of container delivery to --> 

              <div class="uk-container">
                    <div class= uk-grid>
                        
                            <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1" ID="CustDetail" style="display:none;"></div> 
              </div>
                    </div> <!--  end of customer detail  -->

   
     <div class="uk-container uk-text-center">
          <div class= uk-grid>
                    <table class="uk-table  uk-table-divider uk-text-left uk-table uk-table-hover uk-table uk-table-striped CustTable" ID= "CustTableByCust" style= "border:2px solid black ">
                  <form class ="uk-form-stacked">
                    <thead>
                        <tr class="row0" style= "border:2px solid black ; height: 70%">
                            <th class="cell00"style= "border:1px solid black">S/N</th>
                            <th class="cell01" style= "border:1px solid black">Out/IN</th>
                            <th class="cell02"style= "border:1px solid black">Form_number</th>
                            <th class="cell03"style= "border:1px solid black">Date_receipt</th>
                            <th class="cell04"style= "border:1px solid black">Item_desc</th>
                            <th class="cell05"style= "border:1px solid black">Quantity</th>
                            <th class="cell06"style= "border:1px solid black">Price</th>
                        </tr>
                    </thead>
                  </form>
                    <tbody>
                     
                     <!-- row = x column=y  ------------>

    <!-- to be append in JavaScript $('#OFScust').change-->


   <!-------end of loop row i ------->
                        
                                                                     
                    </tbody>
                </table>
    </div>
           </div> <!--end of table -->


      
              
                <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-2-3 "><h1 class="uk-card-title">Customer Balace</h1> </div>             
              
                  
                  

             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 " ID = "CusTotal"> </div>
              
          </div>
    </div> <!--end of container Total -->
                  
    </div>
             
                  
              
                    

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>