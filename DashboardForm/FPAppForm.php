<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
      <title>FPOutForSaleAppForm</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      

 </head>
 <body>

      <div class="uk-container ">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3 "> Approve/Update Finish Product <br /> <h1 class="uk-card-title">
                    <?php 
                      switch($_POST['FPTableName']){
                        case "fp_ofs":
                             print  "Out For Sales";
                        break;
                        case "fp_ifs":
                             print  "In From Sale";
                        break;
                        case "fp_ifp":
                           print  " In From Production";
                        break;
                      }
                      ?> </h1>
                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 ">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            

    <div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1 " ID="FPapp_FNum" >
                      <?php 
                        print $_POST['FPToBeApporve'];
                      ?> 
                  </div> <!--We will receive the form number to be approve here  -->
              
          </div>
    </div> <!--end of container Form Number --> 
     
<div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3 ">
                  <fieldset class="uk-fieldset ">

                      <div class="uk-margin ">
                          <input class="uk-input " type="date" ID="ADate" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 ">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->

    <div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Delivery to</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3 "> 
                     <div class="uk-margin">
                                               
                           <select class="chosen-select OFScust " ID="FPAcust" style ="width:100%" > 
                            <option value=""></option>
                                <?php //we return the index $result not the varaible $row as fetch return only 1st row. 
                                      $result = user::CustList();
                                      $FormNum = $_POST['FPToBeApporve'];
                                      $TableName = $_POST['FPTableName'];
                                      if ($TableName == fp_ifp ) {
                                       print "<option value='Production' selected ='selected' >Production</option>"; 
                                      }else {$result1 = user::FindFormReq($FormNum,$TableName);
                                      $Form_Found =  $result1->fetch(PDO::FETCH_ASSOC);
                                      while($row = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                                     if ($row[0] == $Form_Found['Cust_name']){
                                     print "<option value=".$row[0]." selected ='selected' >". $row[1]. "</option>";
                                   }else{
                                     print "<option value=".$row[0].">". $row[1]. "</option>";
                                   }
                                     };
                                     };                                   
                                ?>   
                           </select>                               
                    </div>

                  </div><!-- end of customer select    -->
                  
             
                  <div class='uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 '>منصرف الى </div>              
          </div>
    </div> <!--end of container delivery to --> 

    <div class='uk-container'>
          <div class= uk-grid>
              
                  <div class='uk-card uk-card-default uk-card-body uk-text-center uk-width-1-1 ' ID='CustDetail'>
                    <?php
                    if ($TableName == 'fp_ifp' ) {
                      print "Address = Industry"; 
                     } else {$Cust_detail=user::findCustByName($Form_Found['Cust_name']);
                    print "Address = ".$Cust_detail['Address']." / "."Phone Number = ".$Cust_detail['PhoneNumber']." / "."Contact Person = ".$Cust_detail['ContactPerson'];
                  };
                   ?>
                  </div> 
    </div>
          </div> <!--  end of customer detail  -->


     <div class="uk-container uk-text-center">
          <div class= uk-grid>
                    <table class="uk-table  uk-table-divider uk-text-center OFSTable " ID= "FPATable" style= "border:2px solid black">
                  <form class ="uk-form-stacked">
                    <thead>
                        <tr class="row0" style= "border:2px solid black ; height: 70%">
                            <th class="Acell00"style= "border:1px solid black">S/N</th>
                            <th class="Acell01" style= "border:1px solid black">Code</th>
                            <th class="Acell02"style= "border:1px solid black">Item Description</th>
                            <th class="Acell03"style= "border:1px solid black">Quantity</th>
                            
                        </tr>
                    </thead>
                  </form>
                    <tbody>
                     
                     <!-- row == i ------------>
<?php
     
     $i = 1;
     $result1 = user::FindFormReq($FormNum,$TableName);
    while( $row1 = $result1->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
      
      print "<tr>
      <td ID='Acell".$i."0'>".$i."</td>  
      <td ID='Acell".$i."1'>".$row1[6]."</td>
      <td ID='Acell".$i."2'>
       <select  class='chosen-select' ID= 'SAcell".$i."2'style ='width:60%' >
              <option ></option>" 
              
            ?>   
            <?php 
                  $result3 = user::FPList();
                  
                  while($row = $result3->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                 if ($row[0] == $row1[6]){
                    print "<option value=".$row[0]." selected ='selected' >". $row[1]. "</option>";
                 }else{
                    print "<option value=".$row[0].">". $row[1]. "</option>";
                 };
                 }; 
                          
           
       print "</select>   
       </td>

      <td ID='Acell".$i."3' >
           <div class='uk-margin'>
              <div class='uk-inline' > 
                  <a class='uk-form-icon' href='#'' uk-icon='icon: pencil'></a>
                  <input class='uk-input' ID ='IAcell".$i."3'  type='text' style='width: 60%' value ='".$row1[7]."'>
              </div> 
          </div>

        </td>

                                 
    </tr> ";
     $i++;
    ;}

    ?>
    
    <!-------end of loop row i ------->
                        
                                                                     
                    </tbody>
                </table>
    </div>
           </div> <!--end of table -->


      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3 ">Stor Manager Signature <br/> <br/> .......................................</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                    
                    <button class="uk-button uk-button-default FPApRj" ID="FPApprove" type="submit" style='margin: 2px;'>Approve Request</button>
                    <button class="uk-button uk-button-default FPApRj" ID="FPReject" type="submit" style='margin: 2px;'>Reject Request</button>
                   
                    <button class="uk-button uk-button-default " type="button" style='display:none; margin: 2px;' ID="NA" >Go to Approval List?</button>

                    <div class="uk-card uk-card-default uk-card-body uk-text-right" style='display:none;' ID= 'TableName'  ><?php print $TableName ?> </div>


                  </div>
                  

             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3 ">Receiver Signature <br/> <br />................................... </div>
              
          </div>
    </div> <!--end of container header -->            

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>