
<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
<!DOCTYPE html>
<html>
  <head>
      <title>Project Diagram</title>
      <link rel="shortcut icon" type="image/x-icon" href="/tool/photo/favicon.ico" />


      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=0.5">
  	  <link rel="stylesheet" href="../tool/css/uikit.min.css" />
  	  <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
  	  <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
	   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">  <!-- link that to be able to use icon -->  
 </head>
 <body >
      <div class="uk-child-width-1-1  uk-grid">
    <div>
       <div class="uk-background-cover uk-height-1-1 uk-panel uk-flex uk-flex-center uk-flex-large" style="background-image: url(/tool/photo/147899.jpg);"> 
                    

      <div class="mermaid">
        graph TD
            IN(fa:fa-project-diagram Raw Materail)-->A1(fa:fa-sign-in-alt Login)
            IN(fa:fa-project-diagram Raw Materail)-->A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->|if not|A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->A(fa:fa-industry Industrial Dashboard)
            A2(fa:fa-registered register)-->A(fa:fa-industry Industrial Dashboard)
            A(fa:fa-industry Industrial Dashboard)-->B[fa:fa-barcode Raw Material]
            A(fa:fa-industry Industrial Dashboard)-->C[fa:fa-lightbulb Finish Product]
            A(fa:fa-industry Industrial Dashboard)-->D[fa:fa-theater-masks Customer]
            A(fa:fa-industry Industrial Dashboard)-->D1[fa:fa-people-carry Employee]

            B[fa:fa-barcode Raw Material]-->L[fa:fa-plus-circle Add new Raw Material]
            B[fa:fa-barcode Raw Material]-->E[fa:fa-handshake Request From Production]
            E[fa:fa-handshake Request From Production]-->F[By Item]
            E[fa:fa-handshake Request From Production]-->G[By Finish Product]
            F[fa:fa-list-ol By Item]-->H[Approved By W/H]
            G[fa:fa-list-ol By Finish Product]-->H[Approved By W/H]
            B[fa:fa-barcode Raw Material]--> I[fa:fa-balance-scale Balance Report]
            I[fa:fa-balance-scale Balance Report]-->J[fa:fa-list-ul List Of All Item]
            I[fa:fa-balance-scale Balance Report]-->K[fa:fa-list-ul By Group]
            I[fa:fa-balance-scale Balance Report]-->M[fa:fa-lightbulb by finish Product]
                   
            click A "/dashboard.php"
            click F "/DashboardForm/RMProdReqForm.php"
            click G "/DashboardForm/RMProdReqByBulbTForm.php"
            click IN "/index.php"
            click A1 "/login.php"
            click A2 "/register.php"
            click H "/DashboardForm/RMNeedForApprovalList.php"
            click J "/DashboardForm/RMReportAll.php"
            click K "/DashboardForm/RMReportByGroup.php"
            click M "/DashboardForm/RMReportByFP.php"
            click L "/DashboardForm/AddNewRawMaterial.php"
        </div>

        </div>
    </div>
  </div>  
       
       <div class="uk-child-width-1-1 uk-height-1-1 uk-grid">
    <div>
       <div class="uk-background-cover uk-height-1-1 uk-panel uk-flex uk-flex-center uk-flex-large" style="background-image: url(/tool/photo/147899.jpg);"> 

        <div class="mermaid">
        graph TD
            IN(fa:fa-project-diagram Finish Product)-->A1(fa:fa-sign-in-alt Login)
            IN(fa:fa-project-diagram Finish Product)-->A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->|if not|A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->A(fa:fa-industry Industrial Dashboard)
            A2(fa:fa-registered register)-->A(fa:fa-industry Industrial Dashboard)
            A(fa:fa-industry Industrial Dashboard)-->B[fa:fa-barcode Raw Material]
            A(fa:fa-industry Industrial Dashboard)-->C[fa:fa-lightbulb Finish Product]
            A(fa:fa-industry Industrial Dashboard)-->D[fa:fa-theater-masks Customer]
            A(fa:fa-industry Industrial Dashboard)-->D1[fa:fa-people-carry Employee]
            
            C[fa:fa-lightbulb Finish Product]-->W[fa:fa-plus-circle Add new Finish Product]
            C[fa:fa-lightbulb Finish Product]-->L[fa:fa-handshake Request Sale FP OUT]
            C[fa:fa-lightbulb Finish Product]-->P[fa:fa-handshake Request Sale FP IN]
            C[fa:fa-lightbulb Finish Product]-->Q[fa:fa-handshake Request Production FP IN]
            C[fa:fa-lightbulb Finish Product]-->M[fa:fa-balance-scale Balance]
            L[fa:fa-handshake Request Sale FP OUT]-->V[fa:fa-list-ul appoved By W/H]
            P[fa:fa-handshake Request Sale FP IN]-->V[fa:fa-list-ul appoved By W/H]
            Q[fa:fa-handshake Request Production FP IN]-->V[fa:fa-list-ul appoved By W/H]

            click A "/dashboard.php"
            click IN "/index.php"
            click A1 "/login.php"
            click A2 "/register.php"
            click L "/DashboardForm/FPOutForSaleReqForm.php"
            click P "/DashboardForm/FPInForSaleReqForm.php"
            click Q "/DashboardForm/FPInForProReqForm.php"
            click M "/DashboardForm/FPreport.php"
            click V "/DashboardForm/FPNeedForAppovalList.php"
            click W "/DashboardForm/AddNewFinshProduct.php"
             
        </div>  

</div>
    </div>
  </div>

<div class="uk-child-width-1-1 uk-height-1-1 uk-grid">
    <div>
       <div class="uk-background-cover uk-height-1-1 uk-panel uk-flex uk-flex-center uk-flex-large" style="background-image: url(/tool/photo/147899.jpg);"> 

        <div class="mermaid">
        graph TD
            IN(fa:fa-project-diagram Customer )-->A1(fa:fa-sign-in-alt Login)
            IN(fa:fa-project-diagram Customer )-->A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->|if not|A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->A(fa:fa-industry Industrial Dashboard)
            A2(fa:fa-registered register)-->A(fa:fa-industry Industrial Dashboard)
            A(fa:fa-industry Industrial Dashboard)-->B[fa:fa-barcode Raw Material]
            A(fa:fa-industry Industrial Dashboard)-->C[fa:fa-lightbulb Finish Product]
            A(fa:fa-industry Industrial Dashboard)-->D[fa:fa-theater-masks Customer]
            A(fa:fa-industry Industrial Dashboard)-->D1[fa:fa-people-carry Employee]
            
            D[fa:fa-theater-masks Customer]-->E[fa:fa-plus-circle add new customer]
            D[fa:fa-theater-masks Customer]-->K[fa:fa-print Receipt Save/print]
            D[fa:fa-theater-masks Customer]-->F[fa:fa-money-bill-wave Cashflow]
            D[fa:fa-theater-masks Customer]-->J[fa:fa-balance-scale report]
            J[fa:fa-balance-scale report]-->H[By Customer]
            J[fa:fa-balance-scale report]-->I[All Customer]
                   
            click A "/dashboard.php"
            click IN "/index.php"
            click A1 "/login.php"
            click A2 "/register.php"
            click K "/DashBoardForm/FPNeedForCustReceiptList.php"
            click H "/DashBoardForm/CustomerReportByCust.php"
            click I "/DashBoardForm/CustomerListReport.php"
            click F "/DashBoardForm/CustomerCashIN.php"
            click E "/DashboardForm/AddNewCustomer.php"
        </div>  

</div>
    </div>
  </div>


  <div class="uk-child-width-1-1 uk-height-1-1 uk-grid">
    <div>
       <div class="uk-background-cover uk-height-1-1 uk-panel uk-flex uk-flex-center uk-flex-large" style="background-image: url(/tool/photo/147899.jpg);"> 

        <div class="mermaid">
        graph TD
            IN(fa:fa-project-diagram Customer )-->A1(fa:fa-sign-in-alt Login)
            IN(fa:fa-project-diagram Customer )-->A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->|if not|A2(fa:fa-registered register)
            A1(fa:fa-sign-in-alt Login)-->A(fa:fa-industry Industrial Dashboard)
            A2(fa:fa-registered register)-->A(fa:fa-industry Industrial Dashboard)
            A(fa:fa-industry Industrial Dashboard)-->B[fa:fa-barcode Raw Material]
            A(fa:fa-industry Industrial Dashboard)-->C[fa:fa-lightbulb Finish Product]
            A(fa:fa-industry Industrial Dashboard)-->D[fa:fa-theater-masks Customer]
            A(fa:fa-industry Industrial Dashboard)-->E[fa:fa-people-carry Employee]
            
             
            E[fa:fa-people-carry Employee]-->G[Add new Employee]
            G[Add new Employee]-->L[Employee private:Salary/Vacation#]
            E[fa:fa-people-carry Employee]-->I[Update time sheet by day include Bonus and Sanctions]
            E[fa:fa-people-carry Employee]-->J[Update Employee Vacation]
            E[fa:fa-people-carry Employee]-->M[Yearly National Vacation update]
            L[Employee private:Salary/Vacation#]-->K[Salary Report]
            I[Update time sheet by day include Bonus and Sanctions]-->K[Salary Report]
            M[Yearly National Vacation update]-->K[Salary Report]
            E[fa:fa-people-carry Employee]-->F[Report List of Employee]
            E[fa:fa-people-carry Employee]-->H[Report Employee Time sheet ]

            
                   
            click A "/dashboard.php"
            click IN "/index.php"
            click A1 "/login.php"
            click A2 "/register.php"
            
        </div>  

</div>
    </div>
  </div>
        <script src="/tool/chart/mermaid.js"></script>
        <script>mermaid.initialize({startOnLoad:true});</script>
        <script>mermaid.initialize({theme:'forest'});</script>
    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
                    
  </body>
</html>