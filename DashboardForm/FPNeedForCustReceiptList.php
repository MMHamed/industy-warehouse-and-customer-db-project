<?php
   // if that variable is not define the site will be down. 
   //It allow the config for security
   define ('_CONFIG_',true);
   //upload the config file here once
   require_once '../inc/config.php';
  ?>
      <title>FPNeedForCustReceiptList</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="../tool/css/uikit.min.css" />
      <link href="/tool/chosen/docsupport/style.css" rel="stylesheet" />
      <link rel="stylesheet" href="../tool/chosen/chosen.min.css" />
    
      

 </head>
 <body>

      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Logo</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3"> Finish Product request without receipt List</div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">Company Name & Address</div>
              
          </div>
    </div> <!--end of container header -->            
<div class="uk-container">
          <div class= uk-grid>
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Date</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                  <fieldset class="uk-fieldset">

                      <div class="uk-margin">
                          <input class="uk-input" type="date" ID="Date" value="<?php echo date('Y-m-d'); ?>">
                      </div> 

                   </div>
                  
             
                  <div class="uk-card uk-card-default uk-card-body uk-text-right uk-width-1-3">التاريخ</div>              
          </div>
    </div> <!--end of container Form Date -->

   
     <div class="uk-container uk-text-center">
          <div class= uk-grid>
                    <table class="uk-table  uk-table-divider uk-text-left uk-table uk-table-hover uk-table uk-table-striped OFSTable" ID= "myTable" style= "border:2px solid black ">
                  <form class ="uk-form-stacked">
                    <thead>
                        <tr class="row0" style= "border:2px solid black ; height: 70%">
                            <th class="cell00"style= "border:1px solid black">S/N</th>
                            <th class="cell00"style= "border:1px solid black">Type</th>
                            <th class="cell01" style= "border:1px solid black">Form Number </th>
                            <th class="cell02"style= "border:1px solid black">Customer Name</th>
                            <th class="cell03"style= "border:1px solid black">Date of the appoved request</th>
                            
                        </tr>
                    </thead>
                  </form>
                    <tbody>
                     
                     <!-- row = x column = y  ------------>

    <?php
    
    $Result = user::FPOutNoReceipt();
    $i ='1';
    while($Request_found = $Result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){

      print 
      "<tr>
      <td ID='cell".$i."0'>".$i ."</td>
      <td ID='cell".$i."0'>Finish Product OUT</td> 
      <td ID='cell".$i."1'>  
          <form method='post' action='ReceiptForm.php'>
           <input class='uk-input uk-form-width-medium' type='text' value='fp_ofs' name='FPTableName' style='display:none;'>
           <input class='uk-input uk-form-width-medium' type='submit' value='".$Request_found[0]."' name='FPToBeApporve'>
          </form>
      </td>
      <td ID='cell".$i."2'>".$Request_found[1]."</td>
      <td ID='cell".$i."3'>".$Request_found[2]."</td> 
                            
    </tr> ";
         $i++;
       };

    $Result = user::FPInNoReceipt();
    $i ='1';
    while($Request_found = $Result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){

      print 
      "<tr>
      <td ID='cell".$i."0'>".$i ."</td>
      <td ID='cell".$i."0'>Finish Product IN</td> 
      <td ID='cell".$i."1'>  
          <form method='post' action='ReceiptForm.php'>
           <input class='uk-input uk-form-width-medium' type='text' value='fp_ifs' name='FPTableName' style='display:none;'>
           <input class='uk-input uk-form-width-medium' type='submit' value='".$Request_found[0]."' name='FPToBeApporve'>
          </form>
      </td>
      <td ID='cell".$i."2'>".$Request_found[1]."</td>
      <td ID='cell".$i."3'>".$Request_found[2]."</td> 
                            
    </tr> ";
         $i++;
       };  
    ?>


   <!-------end of loop row i ------->
                        
                                                                     
                    </tbody>
                </table>
    </div>
           </div> <!--end of table -->


      <div class="uk-container">
          <div class= uk-grid >
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Stor Manager Signature <br/> <br/> .......................................</div>
              
              
                  <div class="uk-card uk-card-default uk-card-body uk-text-center uk-width-1-3">
                                      
                  </div>
                  

             
                  <div class="uk-card uk-card-default uk-card-body uk-text-left uk-width-1-3">Receiver Signature <br/> <br />........................................ </div>
              
          </div>
    </div> <!--end of container header -->            

    <?php 
          require_once '../inc/footer.php';
    ?> <!-- this to request the jquery and the uikit.js -->    
      
          
  </body>
</html>